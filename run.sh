
folder=/mnt/nfs/rawdata/Novaseq/190329_A00597_0025_BH7LWFDRXX

python3 illumina_demultiplexing.py ${folder}/SampleSheetBcl2Fastq2_H7LWFDRXX.csv \
--additional-sheets Block-1_43320 ${folder}/25-mrt-2019_PB1750_43320_TvdV_F088-38_Strain_collection_Sequencing.xlsx \
Block-2_43321 ${folder}/25-mrt-2019_PB1752_43321_MJN_F088-38_Strain_collection_Sequencing.xlsx \
NGS_190325_pellet_43674 ${folder}/27-mar-2019_plate_43674_PB1757_LeS_F088-38_Strain_collection_Sequencing.xlsx \
--filters 110930 1500 110039 6000 110984 1500 110825 5000 111359 3000 110767 500 111167 10000 111074 500 111699 1000 111489 17500 111467 2000 \
--rivm-projects 113173