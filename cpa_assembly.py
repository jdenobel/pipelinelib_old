""" CPA Assembly Pipeline.

Wrapper script over the cpa pipeline.
The script assumes that it is being called from the directory where all the
data is located i.e. /mnt/nfs/rawdata/NGS-projects/###/
"""
import argparse
import glob
import os
import subprocess
from utils.file import FileMover

PROJECT_FOLDER = os.getcwd()
OP_ABS_PATH = os.path.realpath('/mnt/web01/output_unchecked')
OP_REPORT_DIR = CPA_REPORT_DIR = 'reports'
CPA_RESULTS_DIR = OP_DESTINATION_DATA = 'cpa_analysis'
CPA_SOURCE_REPORTS_DIR = 'cpa_assembly'
CPA_RESULT_FILE_SUFFIX = '_scaffold-sequences.fna'
# we expect that the inner scripts create a metaspades somewhere in the project
# folder i.e. /mnt/.../XXX_<name>/cpa_analysis/<sample>/metaspades
METASPADES_DIRNAME = 'metaspades'
# name of the main inner script.
CPA_SCRIPT_PATH = '/mnt/nfs/pipelines/cpa_for_dupont/run_cpa.py'



def get_arguments():
    """Gather the required arguments.

    Same as the arguments in CPA_SCRIPT_PATH
    """
    parser = argparse.ArgumentParser(description=__doc__)
    # TODO: update help text on the project argument.
    parser.add_argument('project', type=str,
                        help="Project id. Use 0 for skipping transfer of "
                             "files to Order Portal")
    parser.add_argument('files', type=str, nargs='+',
                        help='Path(s) to Illumina paired-end reads. Files can'
                             'be compressed')
    parser.add_argument('-l', '--minscflen', default='1000',
                        help='Minimum scaffold length (default = 1000)')
    parser.add_argument('-n', '--nproc', type=str, default='6',
                        help='Number of processors to use (default = 6)')
    parser.add_argument('-e', '--nnode', type=str, default='5',
                        help='Maximum number of nodes to use (default = 5)')
    parser.add_argument('-d', '--oldnaming', action='store_true',
                        help='When used, treats read file names as old BC '
                             'file names to derive the sample name')
    parser.add_argument('-m', '--maindir', default='cpa_analysis',
                        help='The main output dir (default = '
                             'cpa_analysis)')
    parser.add_argument('-k', '--keep', action='store_true',
                        help='When used, keeps intermediate files (default ='
                             'false)')
    parser.add_argument('-s', '--spades-args', type=str,
                        nargs=argparse.REMAINDER,
                        help="extra arguments for the metaspades task i.e. "
                             "--only-assembler -x value")
    return parser.parse_args()

def move_results_to_OP(project, files_expression, data_dir=CPA_RESULTS_DIR,
                       report_dir=CPA_REPORT_DIR,
                       data_result_suffix=CPA_RESULT_FILE_SUFFIX,
                       project_folder=PROJECT_FOLDER):
    """Move results and reports to the Order Portal.

    The data is expected to be in:
    "PROJECT_FOLDER/CPA_RESULT_DIR/sample_name/METASPADES_DIRNAME"
    The only file that needs to be moved from there is the
    CPA_RESULT_FILE_PATTERN

    Reports are located in:
    "PROJECT_FOLDER/project/CPA_REPORT_DIR/"
    from this folder, all the xmls are copied.

    Arguments:
        * project (str): a string representing a project number
        * files (lst): a list of path expression representing the sample files.
            Will use this to extract sample names to be moved to the OP
        * data_dir (str): relative path to the results of the pipeline.
            defaulting to CPA_RESULTS_DIR
        * report_dir (str): relative path to the reports of the pipeline.
            defaulting to CPA_REPORT_DIR.
        * filename_pattern (str): a pattern for the data file.
    """
    files = []
    for expression in files_expression:
        files.extend(glob.glob(expression))
    # sample names are the first character after the underscore.
    samples = [
        os.path.basename(file).split("_")[0] for file in files if not'BC_blanc' in file
        ]
    target_data_dir = os.path.join(OP_ABS_PATH, project,
                                   OP_DESTINATION_DATA)
    target_report_dir = os.path.join(OP_ABS_PATH, project,
                                     OP_REPORT_DIR)
    for sample in samples:
        data_file = f'*{data_result_suffix}'
        report_file = f'{sample}_cpa-assembly-report.xml'

        source_data_dir = os.path.join(project_folder, data_dir,
                                       sample, METASPADES_DIRNAME)
        data_handler = FileMover(source_data_dir, target_data_dir)
        data_moved = data_handler.move_with_pattern(data_file, copy=True)

        source_report_dir = os.path.join(project_folder, report_dir,
                                         CPA_SOURCE_REPORTS_DIR)
        report_handler = FileMover(source_report_dir, target_report_dir)
        report_moved = report_handler.move_with_pattern(report_file, copy=True)
        print(f'{data_moved} data file(s) and {report_moved} report(s) were '
              f'moved to the OP for sample {sample}.')


if __name__ == '__main__':
    """Entry point of script."""
    arguments = get_arguments()
    minscflen = f" -l {arguments.minscflen}" if arguments.minscflen else ""
    nproc = f" -n {arguments.nproc}" if arguments.nproc else ""
    nnode = f" -e {arguments.nnode}" if arguments.nnode else ""
    oldnaming = f" -d {arguments.oldnaming}" if arguments.oldnaming else ""
    maindir = f" -m {arguments.maindir}" if arguments.maindir else ""
    keep = f" -k {arguments.keep}" if arguments.keep else ""
    spades_extra = f" -s {' '.join(arguments.spades_args)}"\
                    if arguments.spades_args else ""
    # only use this on Unix subsystems.
    command = (f"python2 {CPA_SCRIPT_PATH} "
               f"-p {arguments.project} "
               f"-f {' '.join(arguments.files)} "
               f"{minscflen}{nproc}{nnode}{oldnaming}{maindir}{keep}"
               f"{spades_extra}")
    print(f"calling command: '{command}'")
    process = subprocess.Popen(command, shell=True)
    outs, errs = process.communicate()
    move_results_to_OP(arguments.project, arguments.files)
