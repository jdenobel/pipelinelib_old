#!/usr/bin/env python3
"""
TODO:
    ~ python script:
        Multi-sample execution
        arguments:
            * project
            * minscflen
            * nproc
            * nnode
            * oldnaming
            * maindir
            * plasmid
            * keep
            * reads (nargs)

        for each sample:  
            make log file
            call perl main analysis
            call perl stats           

    ~ Perl script:
        Single sample execution
        arguments:  
            * $minctglen, 
            * $misassembly, 
            * $nproc, 
            * $sname, 
            * $maindir, 
            * $plasmid    

        preprocess:
            * In main output dir, make sample dir
            * goto sample dir.
            * make statistics folder
            * make tmp folder
            * make spades folder   
    
        Five tasks:
            1. spades:
                * spades.py --plasmid --pe1-1 <R1 file>  --pe1-2 <R2 file>\ 
                 -t <nproc> -o <spades folder>
                * get output (spades folder, /contigs.fasta)
                * get insert size from (spades folder, /spades.log):
                    Insert size = (\d+).+, deviation = (\d+).+, left quantile/)
                * format command for SSPACE:
                    libPE bowtie $r1_full_path $r2_full_path $insert_size 0.75 "FR"
            2. SSPACE:
                * perl /pipeline/SSPACE/SSPACE_bowtie2.pl -s <spades output> -l 
                    -T <nproc> -z minctglen -b $sspace_folder > $tmp_folder/sspace_logfile.txt"

                * copy ("$sspace_folder/$sspace_folder.summaryfile.txt", 
                    "$stats_folder/$sspace_folder.scaffold-summaryfile.txt"

                * remove $sspace_folder/reads $sspace_folder/alignoutput $sspace_folder/pairinfo

                * SSPACE_scaffolds = $sspace_folder/$sspace_folder.final.scaffolds.fasta

            3. Pilon:
                * mkdir Pilon
                * $numGaps = getNumGaps($SSPACE_scaffolds);
                * if numGaps > 1:
                    perl /pipeline/GapFiller/GapFiller.pl -s $SSPACE_scaffolds -l \
                        $libfile -T 10 -i 2 -b $gapfiller_folder > $tmp_folder/GapFiller_logfile.txt"
                        copy("$gapfiller_folder/$gapfiller_folder.summaryfile.final.txt", \
                        "$stats_folder/$samplename.GapClosed-summaryfile.txt");       
                    $GapClosed = "$gapfiller_folder/$gapfiller_folder.gapfilled.final.fa";  
                    FormatContigs($GapClosed,"$pilon_folder/$samplename"."_scaffold-sequences.fna");       
                  else:
                    FormatContigs($SSPACE_scaffolds,"$pilon_folder/$samplename"."_scaffold-sequences.fna");   
                * $GapClosed = "$pilon_folder/$samplename"."_scaffold-sequences.fna";

            4. Misassembly (bowtie):
                * make_sys_call("bowtie2-build $GapClosed $pilon_folder/bowInd --quiet") == 0 || die "\nBowtie2-build error; $?"; # returns exit status values
                * make_sys_call("bowtie2 -x $pilon_folder/bowInd -p $nproc -1 $r1_full_path -2 $r2_full_path --quiet | samtools view -S -T $GapClosed -b - > $pilon_folder/$samplename\_alignment.bam");
                * make_sys_call("samtools sort $pilon_folder/$samplename\_alignment.bam $pilon_folder/$samplename\_alignment.sort >/dev/null 2>&1");
                * make_sys_call("samtools index $pilon_folder/$samplename\_alignment.sort.bam");
                * make_sys_call("java -Xmx200g -jar /pipeline/Pilon/pilon-1.21.jar --threads $nproc --genome $GapClosed --fix all --frags $pilon_folder/$samplename\_alignment.sort.bam --changes --output $pilon_folder/$pilon_folder >/dev/null 2>&1");
                * FormatContigs("$pilon_folder/$pilon_folder.fasta", "$samplename\_scaffold-sequences.fna");
                * make_sys_call("rm -rf Pilon/$samplename\_*bam* Pilon/bowInd*");

            5. Generate AGP file:
                * perl /pipeline/fasta2agp.pl $final_scaffold_file $samplename.contig-sequences-gapclosed

            # print final stats                             
"""
import os
from argparse import ArgumentParser


from utils.file import parse_readfile_name
from utils.pipeline import Task, Pipeline
from utils.docker_utils import remove_all_services


ASSEMBLY_IMAGE = "ubuntu" #"image=fs02.compute.local:5000/baseclear/denovo_shortread_assembly:4.0"


parser = ArgumentParser(description = 'This is a Python wrapper the short-read de novo assembly pipeline.')
parser.add_argument('-p', '--project', type = str, required = True,
                    help = 'Project id. Use 0 for skipping the transfer of files to Order Portal (REQUIRED)')
parser.add_argument('-l', '--minscflen', required = False, type=int, default=300, 
                    help = 'Minimum scaffold length (default = 300)')
parser.add_argument('-n', '--nproc', type = int, default =1, required = False,
                    help = 'Number of processors to use (default = 6)')
parser.add_argument('-e', '--nnode', type = int, default =5, required = False,
                    help = 'Maximum number of nodes to use (default = 5)')
parser.add_argument('-m', '--maindir', required = False, default = 'de_novo_short-read_assembly',
                    help = 'The main output dir (default = de_novo_short-read_assembly)')  
parser.add_argument('-z', '--plasmid', required = False, action = 'store_true',
                    help = 'When used, activates plasmid assembly for SPAdes')
parser.add_argument('-k', '--keep', required = False, action = 'store_true',
                    help = 'When used, keeps intermediate files (default = false)')                   
parser.add_argument('fastx_reads', type = str, nargs='+', 
                    help = 'Path(s) to Illumina paired-end reads (REQUIRED. Files can be compressed)')

args = parser.parse_args()


pipeline = Pipeline(
    args.project,
    "de_novo_shortread_assembly"
)



test_wait = Task(
    image_name="ubuntu",
    cpu=args.nproc,
    mounts=("/home/jacob/Data:/tmp:rw",),
    project_id=args.project,
    name="sleeper",
    command="echo 5"
)

spades = Task(
    image_name="spades:0.1",
    cpu=args.nproc,
    mounts=("/home/jacob/Data:/tmp:rw",),
    project_id=args.project,
    name="spades",
    command="spades.py {plasmid} --pe1-1 {r1} --pe1-2 {r2} -t {cpu} -o output"
)   





if __name__ == "__main__":
    plasmid = '--plasmid' if args.plasmid else '' 
    remove_all_services()
    for readfile in filter(
            lambda f: "_R1_" in f, args.fastx_reads):
        data_dir, r1, r2, name = parse_readfile_name(readfile)

        test_wait.execute_sample(name)

        # spades_service = spades.execute_sample(
        #     name, plasmid=plasmid, r1=r1, r2=r2, cpu=args.nproc)
        

        break


