from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from os.path import basename, isfile
import smtplib

HTML_TEMPLATE = """\
    Dear admin, <br><br>
    The following has occured on SMILE:<br>
    {message}<br><br>
    Kind regards,<br>
    The SMILE Server
"""


def send_email(html_message,
               subject,
               receivers,
               attachments=None,
               sender="BaseClear SMILE <smile@baseclear.nl>",
               use_template=True,):

    email = MIMEMultipart('alternative')
    email['Subject'] = subject
    email['From'] = sender
    email['To'] = '; '.join(receivers)

    if use_template:
        html_message = HTML_TEMPLATE.format(message=html_message)

    email.attach(
        MIMEText(html_message, 'html')
    )
    for f in attachments or []:
        if isfile(f):
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
                part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
                email.attach(part)

    server = smtplib.SMTP('baseclear-nl.mail.protection.outlook.com:25')
    server.ehlo()
    server.starttls()
    server.sendmail(sender, receivers, email.as_string())
    server.quit()

