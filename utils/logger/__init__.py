# TODO: use https://docs.python.org/3.7/library/logging.html
import datetime
import sys

class Logger(object):
    '''
    Basic file logger
    TODO:
        inherit from logger
        include sentry
    '''
    def __init__(self, file=None):
        self.file = file
        self.stdout = sys.stdout
        
    def write(self, obj, msg):
        msg = f"[{datetime.datetime.now()}] {msg}\n"
        if self.file:
            with open(self.file, "a+") as f:
                f.write(msg)        
        # obj.write(msg)
        print(msg.strip())


    def log(self, msg):
        self.write(sys.stdout, msg)

    def error(self, err):
        self.write(sys.stderr, f"ERROR: {err}")

    def warn(self, msg):
        self.write(sys.stdout, f"WARNING: {msg}")
