import os
import glob

from time import sleep, time

POLL_TIMEOUT = 10 * 60
DEFAULT_DELAY = 30 * 60


def parse_readfile_name(filename):
    """
    Function for parsing the name of fastq files
    """
    abspath = os.path.abspath(filename)
    inputdir, r1 = os.path.split(abspath)
    assert "_R1_" in filename, f"Incorrect naming of file {r1}"

    r2 = r1.replace('_R1_', '_R2_')
    samplename = '_'.join(r1.split('_')[0:-6])
    if not '_L00' in filename:
        samplename = '_'.join(r1.split('_')[0:-5])

    assert len(samplename) != 0, \
        f"Parsing of {filename} resulted in empty samplename"
    return inputdir, r1, r2, samplename.replace('-', '_')


def get_project_dir(project_number, dirname, ngs_folder, inner_folder="raw_sequences"):
    """Get a directory

    Arguments:
    * project_number: a project_number
    * dirname: a directory in the inner_folder i.e. FCID_Awaiting_additional_data
    * ngs_folder: the basedir of where the project_number is
    * inner_folder: the folder inside project_number
    """
    expr = os.path.join(ngs_folder, f"{project_number}*", inner_folder, dirname)
    dirs = glob.glob(expr)
    return dirs[0] if dirs else None


def wait_for(filename, delay=DEFAULT_DELAY):
    while not os.path.isfile(filename):
        sleep(POLL_TIMEOUT)
    else:
        sleep(DEFAULT_DELAY)
