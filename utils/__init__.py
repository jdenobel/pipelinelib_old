"""Generic implementation of behaviour required by all pipelines.

The available modules are:
* docker
* file
"""
from .docker import *
from .pipeline import *
from .file import *
from .args import *
from .send_email import *
from .functions import *
