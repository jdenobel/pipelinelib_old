import os
import pandas as pd
from glob import glob
from file import *

def test_parse_all_illuminadata():

    root = "/mnt/nfs/rawdata/IlluminaData/"
    for p in os.listdir(root):
        pj = os.path.join(root, p)
        if os.path.isdir(pj) and p.split("_")[0].isdigit():
            try:
                samplesheet = Bcl2FastqSampleSheet([x for x in glob(
                    os.path.join(pj,"SampleSheetBcl2Fastq2*")
                    ) if "checked" not in x.lower()][0])
                if not samplesheet.requires_additional_data:
                    continue
            except:
                continue






                            
            print(samplesheet)
            # f088_sheets = glob(
            #     os.path.join(pj, "*F088*.xlsx")
            # )
            # f089_sheets = glob(
            #     os.path.join(pj, "*F089*.xlsx")
            # )

            # other = [
            #     x for x in glob(
            #         os.path.join(pj, "*.xlsx"))
            #     if x not in f088_sheets \
            #         and x not in f089_sheets \
            #         and "summary-demultiplexing" not in x
            # ]
            
            # if len(f088_sheets) != 0:              
            #     for sheet in f088_sheets:
            #         sheet = F088(sheet)                  
            #         try:
            #             samplesheet.update_data(
            #                 sheet
            #             )
            #         except Exception as err:
            #             print(err)
            #             print(sheet.filename)
            #             print()

           
            # if len(f089_sheets) != 0:              
            #     for sheet in f089_sheets:            
            #         print(sheet)
            #         try:
                        
            #             samplesheet.update_data(
            #                 F089(sheet, "A")
            #             )
            #             print("Working\n\n")
            #         except  ValueError: pass
            #         except Exception as err:
            #             print(err)   
            #             raise err                    
            #             print()
            
def test_nextera():
    folder = '/mnt/nfs/rawdata/RUN_ARCHIVE/Illumina_2018/180810_M01687_0082_000000000-BYGDT'
    samplesheet = Bcl2FastqSampleSheet(os.path.join(folder, 'SampleSheetBcl2Fastq2_BYGDT.csv'))
    ass = Nextera(os.path.join(folder, 'samplesheet_NGS_pool_16004RP.xlsx'), '16004_RP_33896')

    # print(samplesheet.data)
    # print(samplesheet.unseen_plates)
    print(len(samplesheet.data.index))
    samplesheet.update_data(ass)
    print(len(samplesheet.data.index))
    # print(ass.data['Sample_ID'])

if __name__ == "__main__":
    test_nextera()