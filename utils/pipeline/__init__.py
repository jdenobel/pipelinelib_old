# import os
# from utils.docker_utils import dockerize, check_mounts

# class Task(object):
#     def __init__(
#             self, image_name, cpu, mounts, 
#             project_id, name, command,
#             workdir=os.getcwd()):
#         self.image_name = image_name
#         self.cpu = cpu
#         self.mounts = check_mounts(mounts)
#         self.project_id = project_id
#         self.name = name
#         self.command = command
#         # Not sure if needed
#         self.workdir = workdir    

#     def execute_sample(self, sample_name, wait_for=None, *args, **kwargs):
#         @dockerize(
#             self.image_name,
#             name=f"{self.name}_{self.project_id}_{sample_name}",
#             cpu=self.cpu,
#             mounts=self.mounts,
#             wait_for=wait_for,
#             workdir=self.workdir)        
#         def inner(self,  **kwargs):
#             return self.command.format(**kwargs)
#         return  inner(self, **kwargs)



# class Pipeline(object):
#     def __init__(self, project_id, name):
#         self.name = name
#         self.project_id = project_id

#     def add_task(self, *args, **kwargs):
#         self.tasks.append(
#             Task(
#                 *args,
#                 project_id=self.project_id,
#                 **kwargs
#             )
#         )


