import argparse
import os
from glob import glob
from utils.docker import Service

from pdb import set_trace


class Handler:
    image   = "fs02.compute.local:5000/baseclear/pigz:1.0"
    command =  "pigz -d -p 2 -k --force /output/{zipfile}"

    def __init__(self, dirpath, ziptype):
        self.root_dir = os.path.abspath(dirpath)
        self.zipfiles = glob(os.path.join(self.root_dir, f"*{ziptype}"))
        self.relative_paths = [os.path.basename(f) for f in self.zipfiles]
        
    def __call__(self):
        jobs = []
        for f in self.relative_paths:
            service = Service(
                image_name=self.image,
                name=f"unzip_{f}".replace(".", ""),
                ncpu=2,
                verbose=True,
                mounts=(f"{self.root_dir}:/output:rw",)
            )
            jobs.append((service, service.execute(self.command.format(zipfile=f))))
        Service.wait_for_services(jobs)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("dirpath", help="Absolute path to a directory")
    parser.add_argument("-t", "--ziptype", default="gz",
        help="Zipfile type to unzip (default: gz)")
    Handler(**parser.parse_args().__dict__)()



