import os

class BaseParser(object):
    def __init__(self, filename, buffer=None):
        self.filename = filename
        if not buffer and not os.path.isfile(self.filename):
            raise FileNotFoundError(
                f"{self.filename} was not found"
        )

    def __repr__(self):
        return f"<{type(self).__name__}: {os.path.basename(self.filename)}>"
