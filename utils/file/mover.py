import os
import shutil
import glob
import random


class FileMover():
    """This class moves files, and ensures that there is enough data available.

    It also ensure thats the correct directories are created.

    Arguments:
        * source_dir : string (path)
        * target_dir : string (path)
        * free_space_threshold : integer
            threshold for target volume, in gigabytes, if less than
            this threshold is available on the target, don't move.
    """

    def __init__(self, source_dir, target_dir, free_space_threshold=0.0):
        source_dirs = glob.glob(source_dir)  # This needs to exists...
        if not source_dirs:
            raise FileNotFoundError(f"'{source_dir}' directory does not "
                                    f"exists.")
        self.source_dir = source_dirs[0]
        self.target_dir = target_dir
        self.free_space_threshold = free_space_threshold
        if not os.path.isdir(target_dir):
            os.makedirs(target_dir)

    def move(self, filename, copy):
        # we may need to override files when we have new data.
        filename = os.path.basename(filename)
        src = os.path.join(self.source_dir, filename)
        dest = os.path.join(self.target_dir, filename)
        self.check_available_size(src)
        if copy:
            if os.path.isdir(src):
                shutil.copytree(src, dest)
            else:
                shutil.copy(src, dest)
        else:
            if os.path.exists(dest):
                callbck = os.remove if os.path.isfile(dest) \
                    else shutil.rmtree
                try:
                    callbck(dest)
                except:
                    base, ext = os.path.splitext(dest)
                    dest = f"{base}({random.randint(1, 100)}){ext}"
            shutil.move(src, dest)

    def check_available_size(self, src_file):
        statvfs   = os.statvfs(self.target_dir)
        actual    = statvfs.f_frsize * statvfs.f_bfree
        filesize  = os.path.getsize(src_file)
        available = actual - filesize - self.threshold
        if not available > 1.0 * 1024 * 2:
            raise OSError((
                f"Not enough space available at {self.target_dir} "
                f"({self.to_gigs(actual)}GB available) "
                f"Filesize: {self.to_gigs(filesize)} "
                f"Threshold: {self.to_gigs(self.threshold)}GB"
            ))

    @property
    def threshold(self):
        return self.free_space_threshold * (1024 ** 3)

    def to_gigs(self, n_bytes):
        return round(n_bytes / (1024 ** 3), 2)

    def move_multiple(self, files, copy):
        for filename in files:
            self.move(filename, copy)

    def move_with_pattern(self, patterns, copy=False):
        if type(patterns) is list:
            files = []
            for pattern in patterns:
                files.extend(glob.glob(os.path.join(self.source_dir, pattern)))
        else:
            files = glob.glob(os.path.join(self.source_dir, patterns))

        self.move_multiple(files, copy)
        return len(files)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        os.system(f'chmod 775 -R {self.target_dir}')


if __name__ == "__main__":
    pattern = "*.fastq"
    source_dir = os.path.dirname(os.path.realpath(__file__))
    dest_dir = os.path.join(source_dir, "Result")

    with FileMover(source_dir, dest_dir) as handle:
        print(handle.move_with_pattern(pattern))
