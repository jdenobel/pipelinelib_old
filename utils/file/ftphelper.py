import os
import ssl
import smtplib
import atexit
import datetime
import glob
from ftplib import FTP_TLS, FTP  


class ImplicitFTP_TLS(FTP_TLS):
    """FTP_TLS subclass that automatically wraps sockets in SSL to support implicit FTPS."""
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._sock = None

    @property
    def sock(self):
        """Return the socket."""
        return self._sock

    @sock.setter
    def sock(self, value):
        """When modifying the socket, ensure that it is ssl wrapped."""
        if value is not None and not isinstance(value, ssl.SSLSocket):
            value = self.context.wrap_socket(value)
        self._sock = value


class FTPHelper():
    def __init__(self, 
        ftp_host,
        ftp_credentials,
        basedir='',
        implicit_tls=False,
        verbose=True
    ):
        self.implicit_tls = implicit_tls
        self.closed = False
        self.verbose = verbose
        self.basedir = basedir 
        self.ftp_host = ftp_host  
          
        try:
            if self.implicit_tls:
                self.client = ImplicitFTP_TLS()
                self.client.connect(
                    host=ftp_host, port=990, timeout=30)
                self.client.prot_p()
            else:
                self.client =  FTP(ftp_host)
            out = self.client.login(*ftp_credentials)       
            if not out.startswith('230 '):
                raise Exception(
                    f"Couldn't connect to ftp server reason\n:{out}") 

            self.echo(f"Connected to {ftp_host}")
            # [self.echo(line) for line in self.client.getwelcome().splitlines()]
            self.cd(self.basedir)


        except Exception as error:
            self.echo(f"ERROR: {error}")

        atexit.register(self.__del__)

    def cd(self, dirname):      
        if dirname != "":
            try:
                self.client.cwd(dirname)
            except:
                self.cd("/".join(dirname.split("/")[:-1]))
                self.client.mkd(dirname)
                self.client.cwd(dirname)


    def is_dir(self, dirname):
        try:
            self.client.cwd(dirname)
            self.client.cwd('/')
            return True
        except Exception as err:
            return False
    
    def echo(self, msg, *args, **kwargs):
        if self.verbose:          
            print(f"[{datetime.datetime.now()}] {msg}", *args, **kwargs)          
    
    def send(self, filename):
        try:
            with open(filename, "rb") as handle:
                basename = os.path.basename(filename)
                self.echo("Sending: ", basename)
                self.client.storbinary(f"STOR {basename}", handle)
        except Exception as error:        
            self.echo(f"ERROR: {error}")


    def send_with_pattern(self, source_dir, pattern):
        files = glob.glob(os.path.join(source_dir, pattern))
        self.send_files(files)
        return len(files)

    def send_files(self, files):
        for f in files:
            self.send(f)

    def send_directory(self, root):
        basedir = os.path.basename(root)
        try:
            for dirname, dirs, files in os.walk(root):
                relpath = os.path.relpath(dirname, root)
                realpath = os.path.join(root, dirname)
                sub_directory = os.path.join("/", self.basedir, basedir) if relpath == "." else os.path.join("/",
                    self.basedir, basedir, relpath)              
                if not self.is_dir(sub_directory):
                    self.echo("Creating", sub_directory)
                    self.client.mkd(sub_directory)           
                self.client.cwd(sub_directory)
                self.send_files(
                    [os.path.join(realpath, filename) for filename in files]
                )          
        except Exception as error:
            self.echo(f"ERROR: {error}")

    def __del__(self):
        if not self.closed:
            self.echo(f"Closing connection to {self.ftp_host}")
            if self.implicit_tls:
                self.client.close()               
            else:
                self.client.quit()
            self.closed = True

    def __enter__(self):
        return self
    
    def __exit__(self,  exc_type, exc_val, exc_tb):
        self.__del__()
        




if __name__ == "__main__":
    ftps_host = 'ftp.dlptest.com'
    ftps_creds = ('dlpuser@dlptest.com', 'e73jzTRTNqCN9PYAAjjn')
    # ftps_host = 'ftps.baseclear.com'
    # ftps_creds = ('status_check', '9wxFbQrgkM')
    
    with FTPHelper(ftps_host, ftps_creds, basedir="user_uploads") as handle:
        handle.send_directory("/mnt/nfs/pipelines/pipelinelib/utils/logger")

