"""Helps with the parsing of excel sheets."""
import os
import pandas as pd
from collections import defaultdict

if __name__ != "__main__":
    from .baseparser import BaseParser
    from .exceptions import (
        DuplicateDataError,
    )
else:
    from baseparser import BaseParser
    from exceptions import (
        DuplicateDataError,
  )

STATIC =  os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "static"
)

DEFAULT_INDEX_SET_FILE = os.path.join(STATIC, "indexSets.txt")
DEFAULT_INDEX_TAGS_FILE = os.path.join(STATIC, "indexTags.txt")
DEFAULT_NETERA_TAGS_FILE = os.path.join(STATIC, "nexteraTags.txt")


class Excel(BaseParser):

    def __init__(self, filename, sheet_name=None, **kwargs):
        super(Excel, self).__init__(filename)
        has_header = kwargs.pop('has_header_row', True)
        header = 0 if has_header else None
        self.data = pd.read_excel(filename,
                                  sheet_name=sheet_name,
                                  header=header)
        if not sheet_name:
            self.data = max(
                (dataset for dataset in self.data.values()),
                key=lambda x: len(x.index))

    def assert_sheet_has_data(self):
        '''
        TODO: Check if sheet has data, and maybe set sheet
        to a sheet with data if not found.
        '''
        pass


class F088(Excel):
    _required_cols = [
        "Plate position",
        "Sample name",
        "project code (original)",
        "Index",
        "Sample Code (original)"
    ]
    _non_na_cols = [
        "Plate position",
        "Sample name",
        "Index",
    ]

    def __init__(self, filename, plate_id=None):
        super(F088, self).__init__(filename, 'Plate overview')
        self._validate()
        self._load_index_tags()
        self._check_for_repeats()
        self.plate_id = plate_id
        n_missing = self.plate_map[
            ['Plate position', 'Sample_ID', 'Index', 'Index2']
        ].isnull().values.sum()
        if n_missing:
            raise ValueError(f"{n_missing} rows with missing values in {self.filename}")
        if len(self.plate_ids) == 0:
            raise ValueError(f"No plate (sample) ID for {filename}")

    def _check_for_repeats(self):
        repeat_data = pd.read_excel(self.filename, sheet_name="RepeatPool")
        drop_rows = []
        for i, r in repeat_data.iterrows():
            drop_rows.append(i)
            if(r[repeat_data.columns[0]]) == "plate pos" or \
                (r[repeat_data.columns[1]]) == "plate pos":
                break

        repeat_data.columns = repeat_data.iloc[drop_rows[-1]].values
        repeat_data.drop(drop_rows, axis=0, inplace=True)
        isnull = repeat_data["MB 'new' pool"].isnull()
        if any(~isnull):
            self.data = self.data[
                self.data['Plate position'].isin(
                    repeat_data[~isnull]["plate pos"].values)
                ]

    def _load_index_tags(self):
        index_tags = pd.read_csv(DEFAULT_INDEX_TAGS_FILE, sep="\t", names=["Index", "tag"]).dropna()
        self.data = self.data.merge(index_tags, how="left", on="Index")
        self.data['Index2'] = self.data.apply(lambda x: x['tag'][len(x['tag'])//2:],1)
        self.data['Index'] = self.data.apply(lambda x: x['tag'][:len(x['tag'])//2],1)

    def _validate(self):
        missing_cols = (x for x in self._required_cols if x not in self.data.columns.values)
        if any(missing_cols):
            raise ValueError(
                f"Required colums:  {', '.join(missing_cols)} "
                f"were not found in {self.filename}"
            )

        self.data.dropna(subset=self._non_na_cols, inplace=True)

        self.data['Index'] = self.data['Index']\
            .astype(int).astype(str)

        for col in ("Sample name", "Index",):
            duplicates = self.data[col].duplicated()
            if any(duplicates):
                duplicate_samples =\
                    self.data[duplicates][col].values.tolist()
                raise DuplicateDataError(
                    duplicate_samples,
                    message=f"Duplicate {col} found."
                    )

        self.data['Sample name'] = pd.to_numeric(self.data['Sample name'],
                                                 errors='ignore',
                                                 downcast='integer')
        invalid_chars = """[?()\[\]\/\\=+<>:;"',*^|&. ]"""
        self.data['Sample name'] = self.data['Sample name']\
            .astype(str).str.replace(invalid_chars, '', regex=True)
        self.data['project code (original)'] = self.data['project code (original)']\
            .astype(str).str.replace(invalid_chars, '', regex=True)



        self.metadata = defaultdict(list)
        for index, row in self.data.dropna(
                subset=['Unnamed: 12', 'Unnamed: 13']).iterrows():
            self.metadata[row['Unnamed: 12'].replace(" ", "_").lower()].append(
                str(row['Unnamed: 13']).replace(" ", "-")
            )

    @property
    def plate_map(self):
        columns = [
            'Plate position',
            'Sample name',
            'Index', 'Index2',
            "project code (original)",
            "Sample Code (original)"
        ]
        return self.data[columns].rename(
            columns={
                'Sample name': 'Sample_ID',
                'project code (original)': 'Sample_Project',
                "Sample Code (original)":  "Code"
                }
        )

    @property
    def plate_ids(self):
        if self.plate_id:
            return [self.plate_id]
        return self.metadata.get('baseclear_unique_plate_id_(n2)', []) + \
            self.metadata.get("customer_unique_plate_id",  [])


    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        return


class F089(Excel):
    '''
    This is very specific for the baseclear F089 samplesheets, do
    not use this with 'normal' samplesheets
    '''
    # TODO: move to a more generic constant if also used in F088 and NextEra...

    def __init__(self, filename, index_set_id=None, plate_id=None, **kwargs):
        """Initialize an F089 sample sheet."""
        super(F089, self).__init__(filename, None)
        self.plate_id = plate_id
        self.index_set_file = kwargs.get('index_set_file')
        self.index_set_id = self.get_index_set_id() if not index_set_id else index_set_id
        self.index_set = self.get_index_set()
        self.metadata = defaultdict(list)
        for index, row in self.data.dropna(
                subset=['Unnamed: 7', 'Unnamed: 8']).iterrows():
            self.metadata[row['Unnamed: 7'].replace(" ", "_").lower()].append(
                str(row['Unnamed: 8']).replace(" ", "-")
            )

        if len(self.plate_ids) == 0:
            raise ValueError(f"No plate (sample) ID for {filename}")
        self.data.dropna(subset=["Sample name"], inplace=True)
        self.data['Sample name'] = pd.to_numeric(self.data['Sample name'],
                                                 errors='ignore',
                                                 downcast='integer')

    def get_index_set(self):
        '''
        If not plate position. just return map list to index set.
        '''
        file = self.index_set_file if self.index_set_file else DEFAULT_INDEX_SET_FILE
        index_sets = pd.read_csv(file, sep="\t").dropna()
        index_set = index_sets[
                [
                    "PlatePosition",
                    self.index_set_id
                ]
            ].rename(columns={
                'PlatePosition': 'Plate position',
                self.index_set_id: 'Index'
                })

        index_set['Index2'] = index_set['Index'].str[8:]
        index_set['Index'] = index_set['Index'].str[:8]
        self.data = pd.merge(self.data, index_set, on='Plate position')
        return index_set

    @property
    def plate_map(self):
        return self.data[['Plate position','Sample name', 'Index', 'Index2']].rename(
            columns={
                'Sample name': 'Sample_ID'
                }
            )

    @property
    def plate_ids(self):
        if self.plate_id:
            return [self.plate_id]
        return self.metadata.get('baseclear_unique_plate_id_(n2)', []) + \
            self.metadata.get("customer_unique_plate_id",  [])

    def get_index_set_id(self):
        try:
            return "ABCD"[int(str(self.metadata.get("index_set"))[-1])]
        except Exception:
            raise ValueError(f"No index set found for {self.filename}")


class Nextera(Excel):
    def __init__(self, filename, plate_id=None, custom_tags=None, **kwargs):
        """"""
        # WARNING: most of the time, NextEra samplesheets do not contain header
        # columns.
        kwargs['has_header_row'] = False
        super(Nextera, self).__init__(filename, sheet_name=None, **kwargs)

        # Drop all columns more than 80% missing values values
        self.data.dropna(thresh=int(len(self.data.index) * .8), inplace=True, axis=1)

        if not plate_id:
            raise ValueError("Plate ID is required")

        if not len(self.data.columns) == 3:
            raise ValueError("Wrong number of columns found in nextera samplesheet")

        self.data.columns = ["Sample_ID", "Index", "Index2"]
        self.plate_ids = [plate_id]
        self.tags_file = custom_tags if custom_tags else DEFAULT_NETERA_TAGS_FILE

        if not os.path.isfile(self.tags_file):
            raise FileNotFoundError(f"{self.tags_file} was not found")

        self._load_index_tags()
        self._validate()

    def _load_index_tags(self):
        """"""
        if all(self.data['Index'].str.isalpha()):
            if not all(self.data['Index'].str.len() == self.data['Index2'].str.len()):
                raise ValueError("Nextera Index Tags are not of equal length")
        elif all(self.data['Index'].str.isdigit()) or all(self.data['Index'].str.isalnum()):
            index_tags = pd.read_csv(
                self.tags_file, sep="\t", names=["Index", "tag"]).dropna()
            if not len(index_tags.columns) == 2:
                raise ValueError("Wrong number of columns found in index tags file")
            index_mapper = index_tags.set_index("Index")['tag'].to_dict()
            self.data[['Index', "Index2"]] = self.data[['Index', "Index2"]].apply(
                lambda x: [
                    index_mapper.get(x['Index']),
                    index_mapper.get(x['Index2'])
                ], result_type='broadcast', axis=1)
        else:
            raise ValueError(
                "Girl! I can't handle a mix of data types for indices. "
                "Check the Nextera samplesheet"
             )

    def _validate(self):
        self.data.dropna(subset=self.data.columns, inplace=True)

        if self.data['Sample_ID'].dtype == str:
            self.data['Sample_ID'] = self.data['Sample_ID'].str.replace("R1", "r1")
            self.data['Sample_ID'] = self.data['Sample_ID'].str.replace("R2", "r2")

        self.data['Sample_ID'] = pd.to_numeric(self.data['Sample_ID'],
                                               errors='ignore',
                                               downcast='integer')
        invalid_chars = """[?()\[\]\/\\=+<>:;"',*^|&. ]"""
        self.data['Sample_ID'] = self.data['Sample_ID']\
            .astype(str).str.replace(invalid_chars, '', regex=True)


        s_indices = self.data['Sample_ID'].str.startswith("_")
        self.data['Sample_ID'][s_indices] = self.data['Sample_ID'][s_indices].str[1:]
        t_indices = self.data['Sample_ID'].str.endswith("_")
        self.data['Sample_ID'][t_indices] = self.data['Sample_ID'][s_indices].str[:-1]

        self.data['Sample_ID'] = self.data['Sample_ID'] + f"_{self.plate_ids[0]}-" + self.data.index.astype(str).str[:]

        duplicates = self.data['Sample_ID'].duplicated(keep=False)
        if any(duplicates):
            duplicate_sample_ids =\
                self.data[duplicates].values

            raise DuplicateDataError(
                duplicate_sample_ids,
                message="Duplicate sample ids found."
                )

        duplicated_indexes = self.data[['Index', 'Index2']].duplicated(keep=False)
        if any(duplicated_indexes):
            duplicated_indices = '\n'.join(
                    [",".join(x)
                    for x in self.data[
                        duplicated_indexes][['Sample_ID', 'Index', 'Index2']
                        ].values.tolist()]
                )
            raise DuplicateDataError(
                duplicated_indices,
                message="Duplicate Index tag(s) found."
                )
        self.plate_map = self.data
