""""""
import xmltodict

from .baseparser import BaseParser


class RunInfo(BaseParser):
    '''
    This class can be used to parse a runinfo xml
    file produced by a sequencer. Can be used to more easily
    access the xml data.

    Depends: xmltodict, BaseParser
    '''

    def __init__(self, filename):
        super(RunInfo, self).__init__(filename)

        with open(self.filename, 'r') as handle:
            self.runinfo = xmltodict.parse(handle.read()).get("RunInfo").get("Run")

    @property
    def flowcell_id(self):
        flowcell_id = self.runinfo.get("Flowcell")
        return flowcell_id if len(flowcell_id) == 9 else flowcell_id[10:]

    @property
    def is_mySeq(self):
        flowcell_id = self.runinfo.get("Flowcell")
        return len(flowcell_id) != 9 and flowcell_id.startswith("000")

    @property
    def reads(self):
        return self.runinfo.get("Reads").get("Read")

    @property
    def total_cycles(self):
        return sum(int(read.get("@NumCycles")) for read in self.reads)

    @property
    def bases_mask_list(self):
        bases_mask = []
        for read in self.reads:
            n_cycles = int(read.get("@NumCycles"))
            mask = "Y*"
            if read.get("@IsIndexedRead") == 'Y':
                mask = "I6n*" if n_cycles == 7 else f"I{n_cycles}n*"
            bases_mask.append(mask)
        return bases_mask

    @property
    def is_sr(self):
        return len(self.bases_mask_list) < 4 and self.bases_mask_list[1][1] == "6"

    @property
    def bases_mask(self):      
        return f"--use-bases-mask {','.join(self.bases_mask_list)}"


if __name__ == "__main__":
    testfile = "/mnt/nfs/pipelines/pipelinelib/test_data/180903_M01687_0088_000000000-C36TF/RunInfo.xml"
    # testfile = "/mnt/nfs/rawdata/RUN_ARCHIVE/Illumina_2018/180822_D00148_0814_AHLGM5BCX2/RunInfo.xml"
    runinfo = RunInfo(testfile)
    print(runinfo.is_sr)

