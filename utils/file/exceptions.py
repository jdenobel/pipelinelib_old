"""Exceptions used for common errors while parsing files."""


class DuplicateDataError(ValueError):
    """Generic exception that handles duplicates in data."""

    def __init__(self, duplicates, message="Duplicate Values found."):
        """"""
        self.message = f"{message}\n{duplicates}"
        super(DuplicateDataError, self).__init__(self.message)


class NoSampleForPoolException(ValueError):
    """"""

    def __init__(self, pool_projects):
        """When you have a pool in a sample sheet that does not have samples.

        Args:
            pool_projects (:obj:`list`): a list of Projects that have pools.
        """
        self.message = f"No samples found for one of the projects: {pool_projects}.\nYou may have a pool that has no samples."
        super(NoSampleForPoolException, self).__init__(self.message)
