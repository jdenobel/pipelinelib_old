import pandas as pd

from .baseparser import BaseParser

class LaneBarcode(BaseParser):
    def __init__(self, globlist):
        if len(globlist) != 1:
            raise FileNotFoundError(
                f"Wrong number of html files found, {len(globlist)}")
        filename = globlist[0]
        super(LaneBarcode, self).__init__(filename)
        self.tables = pd.read_html(self.filename, header=0)


    def get_table_w_column(self, column):
        for table in self.tables:
            if column in table.columns:
                return table
    @property
    def lane_summary(self):
        data = self.get_table_w_column("Project").rename(
            columns={
                "Sample" : "Sample_ID",
                "Project": "Sample_Project"
            }).copy()

        data[['PF Clusters', "Yield (Mbases)"]] = data[
            ['Sample_ID','PF Clusters', "Yield (Mbases)"]
        ].groupby("Sample_ID").transform('sum')
        data[
            [
                '% of thelane',
                '% Perfectbarcode',
                '% One mismatchbarcode',
                '% PFClusters',
                '% >= Q30bases',
                'Mean QualityScore'
            ]] = data[
            [
                'Sample_ID', '% of thelane',
                '% Perfectbarcode',
                '% One mismatchbarcode',
                '% PFClusters',
                '% >= Q30bases',
                'Mean QualityScore']
        ].groupby("Sample_ID").transform("mean")

        return data.drop_duplicates(subset="Sample_ID")


    @property
    def flowcell_summary(self):
        return self.get_table_w_column("Clusters (Raw)")

    @property
    def unknown_barcodes(self):
        return self.get_table_w_column("Sequence")
