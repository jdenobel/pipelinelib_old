"""Module that helps to parse a bcl2fastq excel sheet.

An excelsheet given to this module must have the following sections:
    * Header
    * Reads
    * Settings
    * Data
"""

# TODO:
#   * document your methods?
import math
import string
import os
from io import StringIO

import xmltodict
import pandas as pd

from .baseparser import BaseParser
from .exceptions import (
    DuplicateDataError,
    NoSampleForPoolException,
)


class Bcl2FastqSampleSheet(BaseParser):
    """"""
    REPORTS_DIR = './reports'
    LANE_PREFIX = 'flowcell_summary'
    _required_columns = [
        "FCID", "Lane","Sample_ID",
        "Index","Index2","Recipe",
        "Operator","Sample_Project"
    ]
    unseen_plates = pd.DataFrame()

    def __init__(self, filename, buffer=None):
        super(Bcl2FastqSampleSheet, self).__init__(filename, buffer)

        read = buffer or open(self.filename)

        with read as handle:
            self.content = handle.read()
            self.data = pd.read_csv(StringIO(
                self.splitted_content.get("Data")
                ))
            self.perform_data_checks()

    def write_raw(self):
        with open(self.filename, "w+") as handle:
            handle.write(self.metadata)
            handle.write("[Data]\n")
            write_data = pd.concat([self.data, self.unseen_plates])
            write_data.to_csv(handle, index=False, na_rep='NA')

    def write(self, run_info=None, suffix='_CHECKED'):
        """Write samplesheets."""
        # Get different indice length from dataframe
        sample_sheets = []
        # didn't work as expected =(
        groups = dict(i for i in self.data.groupby(['IndexLength', 'Index2Length']))
        for index, data in groups.items():
            new_suffix = f"{suffix}{'-'.join(map(str, index))}"
            outfile = f"{self.filename.split('.')[0]}{new_suffix}.csv"
            sample_sheets.append(outfile)
            with open(outfile, "w+") as handle:
                handle.write(self.metadata)
                handle.write("[Data]\n")
                # For Hiseq and Novaseq, we need duplicate lanes for bcl2fastq
                write_data = self.duplicate_lanes(run_info, data) if run_info else data
                write_data.to_csv(handle, index=False, na_rep='NA')
        return sample_sheets

    @property
    def has_index_tags_combinations(self):
        return len(self.data['IndexLength'].unique()) != 1

    def write_excel(self, flowcell_id, basename="summary-demultiplexing"):
        # TODO: override this method to write an q sheet.
        # at the end of the process, for plates, we save the csv to excel
        # when the project has plates, etc
        write_data = self.data
        if basename == "summary-demultiplexing":
            write_data = self.data[
                ["Sample_Project","Code", "Sample_ID", "nrReads", "YieldMb", "avgQ"]].copy()
            write_data[['nrReads', "YieldMb"]] = write_data[
                ['Sample_ID', 'nrReads', "YieldMb"]
            ].groupby("Sample_ID").transform('sum')

            write_data[['avgQ']] = write_data[
                ['Sample_ID','avgQ']
            ].groupby("Sample_ID").transform("mean")

            write_data.columns = [
                "Sample_Project", "Code", "Sample", "Number of reads",
                "Yield (MB) after filtering",
                "Average q-score after filtering"]

        files = []
        for project, group in write_data.groupby("Sample_Project"):
            outfile = os.path.join(self.reports_directory,
                                   f"{project}_{basename}_{flowcell_id}.xlsx")
            files.append(outfile)
            with pd.ExcelWriter(outfile) as handle:
                group.to_excel(handle, index=False, na_rep='NA')

        general_report_file = os.path.join(self.reports_directory,
                                           f"all_projects_{basename}.xlsx")
        with pd.ExcelWriter(general_report_file) as handle:
            write_data.to_excel(handle, index=False, na_rep='NA')
        files.append(general_report_file)
        return files

    @property
    def splitted_content(self):
        splitted = self.content.split("[")
        return {
            splitted[i].split("]")[0]: splitted[i].split("]")[1]
            for i in range(1, len(splitted))
        }

    @property
    def sample_ids(self):
        return self.data.dropna(subset=["FCID"])['Sample_ID'].unique()

    @property
    def data_project_ids(self):
        return self.data.dropna(subset=["FCID"])['Sample_Project'].unique().tolist()

    @property
    def unseen_project_ids(self):
        project_ids = []
        if not self.unseen_plates.empty:
            project_ids = self.unseen_plates.dropna(subset=["FCID"])['Sample_Project'].unique().tolist()
        return project_ids

    def get_meta(self, project_id):
        return self.data.loc[
            (self.data['Sample_Project'] == project_id)
        ].iloc[0]

    @property
    def projects(self):
        # print(self.project_ids)
        return [self.data.loc[
            (self.data['Sample_Project'] == project_id)
        ].iloc[0] for project_id in self.data_project_ids]  +[self.unseen_plates.loc[
            (self.unseen_plates['Sample_Project'] == project_id)
        ].iloc[0] for project_id in self.unseen_project_ids]

    def get_numeric(self, value, keep_float=False):
        """Try to parse to an int.

        * If the value is a string, try to parse to an int, if not successful
          return the value.
        * If the value is a float, try to parse to an int
        * If unable to parse the value then return a 0
        """
        if type(value) is str:
            value = int(value) if value.isdigit() else value
        elif type(value) is float:
            # if the float is nan then return 0
            # if we have a value and we keep the float, then return the float
            # otherwise, return the float parsed as int.
            value = 0 if math.isnan(value) else value if keep_float else int(value)

        return value

    def write_lane_summary(self, outdir, flowcell_id):
        """Write the lane summanry files.

        This file is one of the files that needs to be uploaded to ET (Under
        lane_statistics...)

        Returns: A list of filenames containing summary for lanes.
        """
        for lane, group in self.data.groupby("ET_Lane"):
            filename = f"{self.LANE_PREFIX}_{flowcell_id}_{os.path.basename(outdir)}_lane_{int(lane)}.xml"
            with open(os.path.join(outdir, filename), "w+") as handle:
                sample_list = []
                for index, sample in group.dropna(subset=["FCID"]).iterrows():
                    code = self.get_numeric(sample['Code'])
                    gc = self.get_numeric(sample['GC'])
                    number_reads = self.get_numeric(sample['nrReads'])
                    yieldbases = self.get_numeric(sample['Yield (Mbases)'])
                    yieldmb = self.get_numeric(sample['YieldMb'])

                    sample_dict = {
                        # We get no code for plates...
                        "Code": code,
                        "AverageQualFilt": self.get_numeric(sample['avgQ'], keep_float=True),
                        "FastQCFails": self.get_numeric(sample['fqcFails']),
                        "GCContent": gc,
                        "IndexTag": sample['Barcode sequence'],
                        "Kwaliteitsscore": self.get_numeric(sample['Mean QualityScore'], keep_float=True),
                        "NumberOfReads": number_reads,
                        "PercentagePFClusters": self.get_numeric(sample['% PFClusters'], keep_float=True),
                        "PercentageQ30": self.get_numeric(sample['% >= Q30bases'], keep_float=True),
                        "Project": self.get_numeric(sample['Sample_Project']),
                        "SampleName": sample['ET_sample_name'],
                        "SampleYieldInMBFilt": yieldmb,
                        "SampleYieldInMb": yieldbases,
                        }
                    sample_list.append(sample_dict)
                # write to xml
                # Laan: is the lane of the group.
                samples_lane = self.get_numeric(sample['ET_Lane'])
                handle.write(
                    xmltodict.unparse({
                        "flowcell": {
                            "IlluminaID": flowcell_id,
                            "LaanInfo": {
                                "Laan": samples_lane,
                                "SampleInfo": sample_list
                            }
                        }
                    }, pretty=True)
                )

    def write_sample_xmls(self):
        """Writes reports per sample."""
        for index, sample in self.data.dropna(subset=["Sample_Project"]).iterrows():
            filename = f"{sample['Sample_ID']}_{sample['Sample_Project']}_demultiplexing-report.xml"
            with open(os.path.join(self.reports_directory, filename), "w+") as handle:
                nrReads = self.get_numeric(sample['nrReads'])
                yieldMb = self.get_numeric(sample['YieldMb'])
                handle.write(
                        xmltodict.unparse({
                            "report": {
                                "@type": "NGS Project",
                                "analysis_type": "illumina_demultiplexing",
                                "performed_by": "prod",
                                "project": sample['Sample_Project'],
                                "tables": {
                                    "table": {
                                        "@name" :"statistics after demultiplexing and filtering",
                                        "table_row": {
                                            "sample_name": sample['ET_sample_name'],
                                            "number_of_reads": nrReads,
                                            "yield_in_mbp":  yieldMb,
                                            "average_quality": self.get_numeric(sample['avgQ'], keep_float=True),
                                        },
                                    }
                                }
                            }
                        }, pretty=True)
                )

    @property
    def reports_directory(self):
        directory = os.path.join(
            os.path.dirname(self.filename), self.REPORTS_DIR)
        if not os.path.isdir(directory):
            os.makedirs(directory)
        return directory

    @property
    def instrument_type(self):
        return self.header.get("Instrument Type") or self.header.get("Application") or 'NA'

    @property
    def is_novaseq(self):
        return "novaseq" in self.instrument_type.lower()

    @property
    def metadata(self):
        data = self.splitted_content
        data.pop("Data")
        return "".join(
            f"[{key}]{value}"
            for key, value in data.items()
        )

    @property
    def header(self):
        return self.map_to_dict(self.splitted_content.get("Header"))

    @property
    def settings(self):
        return self.map_to_dict(self.splitted_content.get("Settings"))

    @property
    def reads(self):
        return list(filter(
            None,
            self.splitted_content.get("Reads").splitlines()
            ))

    @staticmethod
    def remove_illegal_chars(series, illegal_chars=None):
        """Only use alphanumeric characters in the series."""
        expression = illegal_chars or "[^a-zA-Z0-9-_]"
        clean_series = series.astype(str).str.replace(expression, '', regex=True)
        return clean_series

    def perform_data_checks(self, after_plate=False):
        """Check sample sheets for abnormalities.

        The data checks focus on checking Bcl2fastq samplesheets.
        """
        # TODO:
        # * each method will modify the self.data dataframe and raise exception

        self._correct_column_data()

        self._check_duplicates(after_plate=after_plate)

        # Assuming that the columns in the list exist.
        self._remove_illegal_characters([
            "Sample_ID",
            "Sample_Project",
            "Operator"
            ])

        self._rename_for_ET()

    # START CHECKING SAMPLESHEET DATA.

    def _check_duplicates(self, after_plate=False):
        """Check for duplicates."""
        duplicated_samples = self.data['Code'].duplicated()

        # Check for duplicates
        duplicates = self.data['Sample_ID'].duplicated()
        if any(duplicates):
            duplicate_sample_ids =\
                self.data[duplicates]['Sample_ID'].values.tolist()
            raise DuplicateDataError(
                duplicate_sample_ids,
                message="Duplicate sample ids found."
                )

        # This needs to check for not same code for plates
        if any(duplicated_samples) and not after_plate:
            # This prevents the pipeline from breaking when it encounters plate samples
            # That have be already added
            if len(self.data[duplicated_samples]['Sample_ID'].unique()) != len(self.data[duplicated_samples]):
                duplicated_sample_names = ','.join(
                    self.data[duplicated_samples]['Sample_ID'].values.tolist()
                    )
                raise DuplicateDataError(
                    duplicated_sample_names,
                    message="Duplicated Sample names found."
                    )

        # check for duplicate index tags
        # We need to check for uniqueness in Lane, Index and Index2.
        unique_columns = ['Lane', 'Index', 'Index2']
        duplicated_indexes = self.data[unique_columns].duplicated(keep=False)
        if any(duplicated_indexes):
            duplicated_indices = '\n'.join(
                    [",".join(x)
                    for x in self.data[duplicated_indexes][['Sample_ID', 'Index', 'Index2']].values.tolist()]
                )
            raise DuplicateDataError(
                duplicated_indices,
                message="Duplicate Index tag(s) found."
                )

        # Need to check if duplication happens on different lanes.
        unique_lanes = self.data['Lane'].unique()
        for lane in unique_lanes:
            list_columns = ['Sample_ID', 'Index', 'Index2']
            lane_data = self.data[self.data['Lane'] == lane]
            data = lane_data[list_columns][[
                'Sample_ID', 'Index', 'Index2']].values.tolist()
            Bcl2FastqSampleSheet._check_collision(data)

    @staticmethod
    def _check_collision(sample_list, allowed_distance=2):
        """Calculate distance between indexes and warn if the're too similar.

        Arguments:
            sample_list (`list`): A list of indices
            allowed_distance (`int`): an allowed distance (default=2)
        """
        collisions = []
        for i in range(len(sample_list)):
            sample = sample_list.pop()
            for compare in sample_list:
                dist1 = Bcl2FastqSampleSheet.get_distance(sample[1], compare[1])
                if dist1 <= allowed_distance:
                    try:
                        dist2 = Bcl2FastqSampleSheet.get_distance(sample[2], compare[2])
                        if dist2 <= allowed_distance:
                            collisions.append(f"{sample} {compare} {dist1} {dist2}")
                    except IndexError:
                        collisions.append(f"{sample} {compare} {dist1}")
        if collisions:
            raise Exception(f"Found collisions: {collisions}")

    @staticmethod
    def get_distance(string1, string2):
        """Calculate distance between indexes."""
        dist = 0
        for j in range(len(string1)):
            try:
                if string1[j] != string2[j]:
                    dist = dist + 1
            except IndexError:
                pass
        return dist

    def _correct_column_data(self):
        """Modify self.data accordingly.

        Mainly modify so it is usable for the demultiplexing process.
        TODO:
        * This method is getting too big. We need to ask lab to simplify?
        """
        # Check if the correct columns are in the samplesheet
        if not set(self._required_columns).issubset(set(self.data.columns)):
            raise ValueError((
                "The following columns weren't found in the samplesheet:\n\t" +
                ", ".join(set(self._required_columns).difference(set(self.data.columns)))
                ))

        # Check for missing index tags, they will be either plates or pools:
        if self.unseen_plates.empty:
            noindex_mask = self.data[["Index", "Index2"]].isnull().any(axis=1)
            self.unseen_plates = self.data[noindex_mask]
            noindex = self.data.loc[noindex_mask]
            self.data = self.data[~noindex_mask]

            ## Very iffy, check this later on! Could be pool is a sample
            # If pool, check if data
            pools = noindex['Sample_ID'].apply(
                    lambda x: 'bc_pool' in x.lower()
                )
            # If no data w. project ID, raise value error
            pool_samples_project = noindex['Sample_Project'][pools]
            projects = self.data['Sample_Project']
            # if you have pools and there are no samples for the pools
            # This should be more robust, it is breaking the automator
            if any(pools) and not any(projects.isin(pool_samples_project)):
                raise(NoSampleForPoolException(pool_samples_project))
            # Others, are plates
            self.unseen_plates = noindex[~pools]

        # Drop duplicate header
        self.data = self.data[self.data['FCID'] != 'FCID']

        # reformat sample names
        if self.data['Sample_ID'].dtype == str:
            self.data['Sample_ID'] = self.data['Sample_ID'].str.replace("R1", "r1")
            self.data['Sample_ID'] = self.data['Sample_ID'].str.replace("R2", "r2")
            ## Remove preceding and trailing underscore
            s_indices = self.data['Sample_ID'].str.startswith("_")
            self.data['Sample_ID'][s_indices] = self.data['Sample_ID'][s_indices].str[1:]
            t_indices = self.data['Sample_ID'].str.endswith("_")
            self.data['Sample_ID'][t_indices] = self.data['Sample_ID'][s_indices].str[:-1]

        # Reassert column type
        for column in self.data.columns:
            try:
                self.data[column] = pd.to_numeric(self.data[column])
            except ValueError:
                continue

        # Correct for duplicate sample names
        if "Code" not in self.data.columns:
            self.data['Code'] = self.data['Sample_ID'].apply(
                    lambda x: str(x).split("_")[-1]
            )

        if "SampleRef" in self.data.columns:
            self.data.loc[self.data['SampleRef'].isna(), 'SampleRef'] = 'noref'
        else:
            self.data['SampleRef'] = 'noref'

        # add the index length to the samplesheet.
        if not self.data['Index'].empty:
            self.data['IndexLength'] =\
                pd.to_numeric(self.data['Index'].str.len(), downcast='integer')

        if not self.data["Index2"].empty:
            self.data['Index2Length'] =\
                pd.to_numeric(self.data['Index2'].str.len(), downcast='integer')

    def _remove_illegal_characters(self, columns):
        """Remove illegal characters from the given column.s"""
        for column in columns:
            self.data[column] = Bcl2FastqSampleSheet.remove_illegal_chars(
                self.data[column])

    def _rename_for_ET(self):
        """Rename based on ET."""
        if 'ET_sample_name' not in self.data.columns:
            self.data['ET_sample_name'] = None
            self.data['ET_Lane'] = None

        sample_list = []
        cols = ['ET_sample_name', 'ET_Lane']
        # samples are the entries that have ET_sample_name and ET_Lane empty.
        samples = self.data[cols].isnull().any(axis=1)
        self.data.loc[samples, 'ET_Lane'] =\
            self.data[samples]['Lane'].astype('int32')
        self.data.ET_sample_name = self.data.ET_sample_name.astype(str)
        for index, row in self.data[samples].iterrows():
            sample_name = row['Sample_ID'].split('_')
            sample_name = "_".join(sample_name[:-1])
            sample_list.append((index, sample_name))
        for index, name in sample_list:
            self.data.at[index, 'ET_sample_name'] = name

        # ensure that ET lanes are kept as integers.
        self.data['ET_Lane'] = pd.to_numeric(self.data['ET_Lane'],
                                             errors='ignore',
                                             downcast='integer')
        # Correct for lanes
        # uncomment for use on runs that should be offsetted by 2.
        # self.data.loc[self.data['Lane'] == 3, 'Lane'] -= 2
        self.data.loc[self.data['Lane'] >= 5, 'Lane'] -= 4

    # END CHECKING SAMPLESHEET DATA.

    @property
    def requires_additional_data(self):
        return len(self.unseen_plates) != 0

    def update_data(self, parser):
        """"""
        plate_mask = self.unseen_plates['Sample_ID'].apply(
            lambda x: \
                any(str(i).lower() in x.lower() for i in parser.plate_ids)
        )
        if not any(plate_mask):
            # This fixes nextera samplesheets
            secondary_mask = self.data['Sample_ID'] == parser.plate_ids[0]
            if not any(secondary_mask):
                raise ValueError(
                    f"{str(parser)} was not compatible with {str(self)}, "
                    f"{' or '.join(parser.plate_ids)} was not found!"
                )
            plate = self.data[secondary_mask].copy()
            if len(plate) != 1:
                raise ValueError(
                    f"{str(parser)} was not compatible with {str(self)}, "
                    f"{' or '.join(parser.plate_ids)} was found {len(plate)} times!"
                )
            self.data = self.data[~secondary_mask]
        else:
            plate = self.unseen_plates[plate_mask].copy()
            if len(plate) != 1:
                raise ValueError(
                    f"{str(parser)} was not compatible with {str(self)}, "
                    f"{' or '.join(parser.plate_ids)} was found {len(plate)} times!"
                )
            self.unseen_plates = self.unseen_plates[~plate_mask]

        # Convert plate map to sample sheet
        newdata = parser.plate_map
        # print(parser.plate_map)
        columns = [
            'FCID',
            'Lane',
            'SampleRef',
            'Control',
            'Recipe',
            'Operator',
            'Sample_Project'
            ]
        for col in columns:
            newdata[col] = plate.loc[plate.first_valid_index(), col]

        newdata['Description'] = plate.loc[plate.first_valid_index(), "Sample_ID"]
        plate_description = plate.loc[plate.first_valid_index(), 'Description']

        if 'Plate position' in parser.plate_map.columns:
            newdata['Sample_ID'] = parser.plate_map.apply(
                lambda x: f"{x['Sample_ID']}_{plate_description}-{x['Plate position']}",
                axis=1)

        self.data = self.data.append(newdata, ignore_index=True, sort=True)

        self.unseen_plates = self.unseen_plates[~plate_mask]

        self.perform_data_checks(True)

    def map_to_dict(self, datastring):
        return {
            section[0]: section[-1]
            for section in (
                line.split(",") for line in filter(
                    None, datastring.splitlines()
                )
            )
        }

    def duplicate_lanes(self, run_info, data):
        """Duplication of lanes.

        Only duplicate lanes when you have Novaseq.
        Will duplicate the lanes for the given data.
        """
        # TODO: No need to duplicate lanes if there is more than one lane
        # planned. You could have lanes 1 throught 4.
        # Check in run_info for amount of lanes used.
        # TODO: If we get an S4, then we need to duplicate from lane 1 all the
        # way to lane 4.
        if 2 not in self.data.Lane.unique() and not run_info.is_mySeq:
            tempdata = data.copy()
            tempdata['Lane'] = 2

            # adding to the main data
            self.data.append(tempdata, ignore_index=True)
            # updating the return value
            data = data.append(tempdata, ignore_index=True)
        return data





if __name__ == "__main__":
    testfiles = [
        # "/mnt/nfs/pipelines/illumina_demultiplexing/180911_M01687_0089_000000000-C34TN/SampleSheetBcl2Fastq2_C34TN.csv",
        "/mnt/nfs/validatie_sets/W-BI-009_Illumina_demultiplexing/161115_D00148_0520_BH7G3YBCXY/SampleSheetBcl2Fastq2_H7G3YBCXY.csv",
        "/mnt/nfs/validatie_sets/W-BI-009_Illumina_demultiplexing/170113_D00148_0546_AH77GTBCXY/SampleSheetBcl2Fastq2_H77GTBCXY.csv",
        "/mnt/nfs/validatie_sets/W-BI-009_Illumina_demultiplexing/170117_D00148_0547_AHG2CVBCXY/SampleSheetBcl2Fastq2_HG2CVBCXY.csv",
        "/mnt/nfs/validatie_sets/W-BI-009_Illumina_demultiplexing/170105_D00148_0543_AHFVN3BCXY/SampleSheetBcl2Fastq2_HFVN3BCXY.csv",
        "/mnt/nfs/validatie_sets/W-BI-009_Illumina_demultiplexing/170113_M01687_0003_000000000-AYRHH/SampleSheetBcl2Fastq2_AYRHH.csv",
        "/mnt/nfs/pipelines/pipelinelib/test_data/180903_M01687_0088_000000000-C36TF/SampleSheetBcl2Fastq2_C36TF.csv",
        '/mnt/nfs/rawdata/Novaseq/180927_A00597_0007_AH7KYWDSXX/SampleSheetBcl2Fastq2.csv'
        # "./test_data/SampleSheet.csv"
    ]


    for f in testfiles:
        parser = Bcl2FastqSampleSheet(f)
        # if parser.requires_additional_data:
        print(f)
        # print(parser.data.head())
        print(parser.is_novaseq)
        # print("header")
        # print(parser.header)
        # print("settings")
        # print(parser.settings)
        # print("reads")
        # print(parser.reads)
        # print("data")
        # print(parser.data['Sample_ID'])
