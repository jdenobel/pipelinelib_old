class Args(dict):
    def __init__(self, adict):
        self.__dict__.update(adict)

    def keys(self):
        return self.__dict__.keys()

    def items(self):
        return self.__dict__.items()

    def values(self):
        return self.__dict__.values()