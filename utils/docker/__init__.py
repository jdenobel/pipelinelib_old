from .service import Service

# import os
# import docker
# import functools
# import random
# import string
# import pwd 
# import time
# from pprint import pprint

# from docker.types import Mount, RestartPolicy

# from pdb import set_trace

# client = docker.from_env()

# def check_mounts(volumes):
#     mounts = [
#         Mount(source=x, target=x,type="bind",read_only=True) 
#         for x in ("/etc/passwd", "/etc/group",)
#     ]
#     for vol in volumes:
#         splitted = vol.split(":")
#         assert len(splitted) == 3, \
#             f"{vol} not in correct format of source:target:options"
#         assert os.path.isdir(splitted[0]), \
#             f"Source dir {splitted[0]}, does not exist"
#         assert splitted[2] in ["ro", "rw"], \
#             f"Mount option {splitted[2]} is invalid"
#         mounts.append(
#             Mount(
#                 source=splitted[0],
#                 target=splitted[1],
#                 type='bind',
#                 read_only=splitted[2] == 'ro'
#             )
#         )
#     return mounts

# def id_generator(size=4, chars=string.ascii_uppercase + string.digits):
#     return ''.join(random.choice(chars) for _ in range(size))

# def remove_all_services(omit=[]):
#     for service in client.services.list():
#         if service.name in omit: continue
#         service.remove()

# def dockerize(image_name, wait=True, wait_for=None, cpu=1, name='', mounts=(), **skwargs):
#     def outer_wrappper(f):
#         @functools.wraps(f)
#         def inner_wrapper(*args, **kwargs): 
#             ## kwargs should be first, 
#             command = f(*args, **kwargs)
#             print(command)
#             service = client.services.create(
#                 image_name, 
#                 command=command,
#                 name=f"{name}_{id_generator()}",
#                 mounts=mounts,
#                 user=f"{os.getuid()}:100",
#                 restart_policy=RestartPolicy(condition='none'),
#                 **skwargs
#             )
#             while len(service.tasks()) < 0:
#                 continue
            
#             task = service.tasks()[0] # zijn er meerderde ofzo
#             while wait and task.get("DesiredState") != 'shutdown': 
#                 # pprint(service.tasks())
#                 task = service.tasks()[0] 

#                 for i in service.tasks():
#                     print(i.get("DesiredState"), task.get("DesiredState"))
#                 # pprint(task)
#                 # import pdb; pdb.set_trace()
#                 print(len(service.tasks()))
#                 time.sleep(1)
            
#             while wait_for != None: pass

#             return service

#         return inner_wrapper
#     return outer_wrappper