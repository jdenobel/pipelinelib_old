#!/usr/bin/python

from __future__ import print_function    
from easydocker import EasyDocker, dockerize
import sys
import socket
import time
import datetime
import os
import random


class MyPipeline(EasyDocker):
    def wrapper(self):
        self.project_id = 12345
        # allows you to specify variables that are serialized along to each container started
        self.context_variables = ['project_id']
        
        # specify volumes (i.e. mounts) for every container
        self.container_volumes = [
            ('/input', 'ro'),
            ('/output', 'rw'),
        ]
        
        print('\nInput and output to the container is forwarded by supplying args + kwargs.')
        my_string = 'Benjamin'
        print(my_string, '-->', self.reverse_string(my_string))
        
        print('\nSequential execution:')
        for i in range(3):
            node_name, container_id = self.get_hostname()
            print('%d. running container [%s] on node [%s]' % (i, container_id, node_name))
        
        print('\nParallel execution 1:')
        for i, node_name, container_id in self.get_hostname_parallel(range(5)):
            print('%d. running container [%s] on node [%s]' % (i, container_id, node_name))
        
        print('\nParallel execution 2:')
        random_sleep_durations = [
            (i, random.randint(2, 5))
            for i in range(5)
        ]
        for iteration_i, container_i in enumerate(self.sleep(random_sleep_durations)):
            print('%d. container %d finished' % (iteration_i, container_i))
        print('>>> Note: even though execution might not proceed in the order as called, return order is still maintained.')
        
        print('\nExecute local as well as in container:')
        print('Wrapper time:', self.get_time(execute_local=True))
        print('Container time:', self.get_time())
        
    @dockerize('ubuntu_with_python')
    def reverse_string(self, string):
        return string[::-1]
    
    @dockerize('ubuntu_with_python')
    def get_time(self):
        return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        
    @dockerize('ubuntu_with_python', volumes=[('/etc/hostname', 'ro')])
    def get_hostname(self):
        with open('/etc/hostname') as f_in:
            return f_in.read().strip(), socket.gethostname()
    
    @dockerize('ubuntu_with_python', volumes=[('/etc/hostname', 'ro')], parallel=True)
    def get_hostname_parallel(self, i):
        with open('/etc/hostname') as f_in:
            return i, f_in.read().strip(), socket.gethostname()
    
    @dockerize('ubuntu_with_python', parallel=True)
    def sleep(self, i, sleep_duration):
        start = time.time()
        print('container %d going to sleep' % i)
        time.sleep(sleep_duration)
        slept_for = time.time() - start
        print('container %d slept_for %f' % (i, slept_for))
        return i
        
        
p = MyPipeline()