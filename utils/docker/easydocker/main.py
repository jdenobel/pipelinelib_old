#!/usr/bin/python

import sys
import argparse
import os
import getpass
import subprocess
import re
import time
import socket
import uuid
import pwd
import grp
import json
import smtplib
import datetime
from email.mime.text import MIMEText
from ServiceHelper import ServiceHelper


"""
To do:
 - --pwd   --> automatic mount of current working directory and cd to it
 - split at image name and do not modify args after it (potential conflict in case of -u arg in entrypoint args)
 - email on completion or error option
 - specify project id; automatic mounting of data dir (ro) and results dir (rw)
 - show my containers (+ only ids)
 - show my images (+ only ids)
 - show all images with authors
 - docker report generator callable from in and outside container
 - automatic resource limiting based on node (e.g. control more restrictions):
    + different limitations per device (e.g. FS02 stricter IO operations than node's local storage)
 - webinterface --> "Docker control panel"
""" 

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
NO_ARG = object()


class Handler(object):
    def __init__(self):
        is_root = (getpass.getuser() == 'root') or (os.getuid() == 0)
        is_admin = (getpass.getuser() == 'admin')
        assert not is_root and not is_admin, 'Do not use the root or admin account to execute Docker commands. Resort to "docker_root" if really necessary.'
        
        self.UID = os.getuid()
        self.GID = pwd.getpwuid(self.UID).pw_gid # User's group id
        self.username = getpass.getuser()
        
        #self.email_on_error = True
        self.dont_mount_passwd = False
        self.email_on_completion = False
        self.verbose = False
        
        args = sys.argv[1:]
        self.via_swarm = False
        
        for arg_name in ('email_on_completion', 'verbose', 'dont_mount_passwd'):
            for i, a in enumerate(args):
                if a.lower().startswith('--%s' % arg_name):
                    if ('=' not in a) or (a.split('=', 1)[-1].lower() == 'true'):
                        setattr(self, arg_name, True)
                        #print('\nOption [%s] is turned on.\n' % arg_name)
                    del args[i]
                    break
        
        if len(args) > 1:
            if args[0:3] == ['-H', ':4000', 'run']:
                self.via_swarm = True
                self.run_container(args[3:])
            
            elif args[0] == 'run':
                return self.run_container(args[1:])
                
            elif args[0:3] == ['service', 'create', 'easy']:
                return self.create_easy_service(args[3:])
            elif args[0:2] == ['service', 'create']:
                return self.create_service(args[2:])
            elif args[0:2] == ['service', 'images']:
                return self.print_service_images()
            elif args[0:3] == ['service', 'rm', '--mine']:
                args.remove('--mine')
                return self.remove_my_services(args[2:])
            
            elif args[0] == 'exec':
                return self.container_exec(args[1:])
            
            elif args[0] == 'build':
                return self.build_image(args[1:])
                
            elif args[0] == 'rm':
                if '--mine' in args:
                    args.remove('--mine')
                    return self.remove_my_containers(args[1:])
        
        if args and args[0] == '--version':
            print('Docker helper script version 31-3-17')
        
        self.execute_docker_command(args)
    
    def print_service_images(self):
        for image_name in json.loads(
            subprocess.check_output(['curl', '--silent', 'fs02.compute.local:5000/v2/_catalog'])
        )['repositories']:
            print('fs02.compute.local:5000/%s' % image_name)
            
    def get_containers(self, only_mine=False):
        docker_columns = ('.ID', '.Image', '.Command', '.CreatedAt', '.RunningFor', '.Ports', '.Status', '.Size', '.Names', '.Labels', '.Mounts')
        pretty_columns = ('id',  'image',  'command',  'created_at', 'running_for', 'ports',  'status',  'size',  'name',   'labels',  'mounts' )
        
        extra_args = [
            '--format', '"{{%s}}"' % '}}\t{{'.join(docker_columns)
        ]
        process = self.execute_docker_command(
            ['ps', '-a'] + extra_args,
            popen_kwargs=dict(
                stdout=subprocess.PIPE
            ),
            exit_afterwards=False
        )
        out, err = process.communicate()
        row_list = [
            [   value.strip()
                for value in row.strip('"').split('\t')
            ]
            for row in out.strip().split('\n') 
            if row.strip().strip('"')
        ]
        
        container_list = []
        for row in row_list:
            container_info = dict(zip(pretty_columns, row))
            container_info['labels'] = dict(
                key_value.split('=')
                for key_value in container_info['labels'].split(',')
                if key_value.strip()
            )
            
            if 'owner' in container_info['labels']:
                container_info['is_mine'] = container_info['labels']['owner'] == self.username
            else:
                container_info['is_mine'] = container_info['name'].startswith(self.username)
            
            try:
                container_info['index'] = int(re.findall('_(\d+)$', container_info['name'])[0])
            except IndexError:
                container_info['index'] = None
            
            if only_mine and not container_info['is_mine']:
                continue
            container_list.append(container_info)
        return container_list
        
    def get_services(self, only_mine=False):
        docker_columns = ('.ID', '.Image', '.Status', '.Names', '.Labels')
        pretty_columns = ('id',  'image',  'status',  'name',   'labels' )
        
        args = [
            'service', 'ls',
            '--format', '"{{%s}}"' % '}}\t{{'.join(docker_columns)
        ]
        
        process = self.execute_docker_command(
            args,
            popen_kwargs=dict(
                stdout=subprocess.PIPE
            ),
            exit_afterwards=False
        )
        out, err = process.communicate()
        row_list = [
            [   value.strip()
                for value in row.strip('"').split('\t')
            ]
            for row in out.strip().split('\n') 
            if row.strip().strip('"')
        ]
        
        service_list = []
        for row in row_list:
            info = dict(zip(pretty_columns, row))
            info['labels'] = dict(
                key_value.split('=')
                for key_value in info['labels'].split(',')
                if key_value.strip()
            )
            
            if 'owner' in info['labels']:
                info['is_mine'] = info['labels']['owner'] == self.username
            else:
                info['is_mine'] = info['name'].startswith(self.username)
            
            try:
                info['index'] = int(re.findall('_(\d+)$', info['name'])[0])
            except IndexError:
                info['index'] = None
            
            if only_mine and not info['is_mine']:
                continue
            service_list.append(info)
        return service_list
            
    def get_next_name_OLD(self):
        container_indices = [
            c['index']
            for c in self.get_containers(only_mine=True)
        ]
        next_index = max(container_indices or [0]) + 1
        return '%s_%d' % (
            self.username,
            next_index
        )
            
    def get_next_name(self):
        return '%s_%s' % (
            self.username,
            str(uuid.uuid4()).replace('-', '')[:20]
        )
    
    @staticmethod
    def process_image_name(args):
        for image_index, a in enumerate(args):
            if a.strip().lower().startswith('image='):
                entrypoint_args = args[image_index + 1:]
                args = args[:image_index + 1]
                args[-1] = args[-1][len('image='):]
                break
        else:
            entrypoint_args = []
        return entrypoint_args
    
    def create_service(self, args):
        entrypoint_args = self.process_image_name(args)
        
        preceding_args = [
            '--label', '"owner=%s"' % self.username
        ]
        if '--name' not in args:
            preceding_args.extend([
                '--name',
                self.get_next_name()
            ])
        if not {'--user', '-u'} & set(args):
            preceding_args.extend([
                '--user', 
                '%s:%s' % (self.UID, self.GID)
            ])
        if '--restart-condition' not in args:
            preceding_args.extend([
                '--restart-condition', 'none'
            ])
        #for g in grp.getgrall():
        #    if self.username in g.gr_mem:
        #        preceding_args.extend([
        #            '--group', g.gr_name 
        #        ])
        
        mounts = [
            '--mount', 'type=bind,src=/etc/group,dst=/etc/group,ro=1',
            '--mount', 'type=bind,src=/etc/localtime,dst=/etc/localtime,ro=1',
            '--mount', 'type=bind,src=%s,dst=%s' % ((os.path.expanduser('~'),) * 2)
        ]
        
        if not self.dont_mount_passwd:
            mounts.extend([
                '--mount', 'type=bind,src=/etc/passwd,dst=/etc/passwd,ro=1',
            ])
        preceding_args.extend(mounts)
        self.execute_docker_command(
            ['service', 'create'] + preceding_args + args + entrypoint_args
        )
    
    def create_easy_service(self, args):
        for arg_i, a in enumerate(args):
            if a.strip().lower().startswith('image='):
                entrypoint_args = args[arg_i + 1:]
                image_name = args[arg_i][len('image='):]
                service_args = args[:arg_i]
                break
        else:
            raise Exception('To use the service helper supply the image name argument like this: image=yourimage')
        
        interactive = False
        for arg_i, a in enumerate(service_args):
            if (not a.startswith('--') and a.startswith('-') and 'i' in a) or (a == '--interactive'):
                interactive = True
                del service_args[arg_i]
                break
        
        service = ServiceHelper(image_name, service_args, verbose=self.verbose)
        service.execute(entrypoint_args, interactive=interactive).wait()
    
    def run_container(self, args):
        entrypoint_args = self.process_image_name(args)
        preceding_args = [
            '--label', '"owner=%s"' % self.username,
        ]
        if '--name' not in args:
            preceding_args.extend([
                '--name',
                self.get_next_name()
            ])
        
        if not {'--user', '-u'} & set(args):
            preceding_args.extend([
                '--user', 
                '%s:%s' % (self.UID, self.GID)
            ])
           
        #for g in grp.getgrall():
        #    if self.username in g.gr_mem:
        #        preceding_args.extend([
        #            '--group-add', g.gr_name 
        #        ])
            
        detached_mode = False
        for a in args:
            if not a.startswith('--') and a.startswith('-') and 'd' in a:
                detached_mode = True
                break
            
        if not any( a.startswith('--rm=') for a in args ) and not detached_mode:
            preceding_args.append('--rm=true')
        
        preceding_args.extend([
            '-v', '/etc/passwd:/etc/passwd:ro',
            '-v', '/etc/group:/etc/group:ro',
            '-v', '/etc/localtime:/etc/localtime:ro',
            '-v', '%s:%s:rw' % ((os.path.expanduser('~'),) * 2)
        ])
        
        self.execute_docker_command(
            ['run'] + preceding_args + args + entrypoint_args
        )

    def container_exec(self, args):
        preceding_args = []
        if not {'--user', '-u'} & set(args):
            preceding_args.extend(
                [
                    '--user', 
                    '%s:%s' % (self.UID, self.GID)
                ]
            )
        self.execute_docker_command(
            ['exec'] + preceding_args + args
        )
    
    def build_image(self, args):
        preceding_args = [
            '--label', '"owner=%s"' % getpass.getuser()
        ]
        if not any( a.startswith('--rm=') for a in args ):
            preceding_args.append('--rm=true')
        self.execute_docker_command(
            ['build'] + preceding_args + args
        )
    
    def remove_my_containers(self, args=None):
        container_ids = [
            c['id']
            for c in self.get_containers(only_mine=True)
        ]
        self.execute_docker_command(['rm'] + (args or []) + container_ids)
    
    def remove_my_services(self, args=None):
        service_ids = [
            c['id']
            for c in self.get_services(only_mine=True)
        ]
        cmd = ['service', 'rm'] + (args or []) + container_ids
        print(cmd)
        self.execute_docker_command(cmd)
    
    def execute_docker_command(self, args, popen_kwargs=None, exit_afterwards=True):
        preceding_args = ['docker']
        
        if self.via_swarm:
            preceding_args.extend([
                '-H', ':4000'
            ])
        
        args_repr = ' '.join(
            '"%s"' % a if ' ' in a else a
            for a in map(str, preceding_args + args)
        )
        
        if self.verbose:
            print('Docker cmd: %s' % args_repr)
        
        if False:
            log_file_path = os.path.join(SCRIPT_DIR, 'docker_logs', time.strftime("%Y-%m-%d.txt", time.gmtime()))
            with open(log_file_path, 'a') as f_out:
                f_out.write(
                    '%s@%s [%s]: %s\n' % (
                        getpass.getuser(),
                        socket.gethostname(),
                        time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()),
                        args_repr
                    )
                )
            os.chmod(log_file_path, 0o777)
        
        container_start = datetime.datetime.now()
        process = subprocess.Popen(
            map(str, preceding_args + args),
            **(popen_kwargs or {})
        )
        exit_code = process.wait()
        
        container_end = datetime.datetime.now()
        container_run_duration = container_end - container_start
        
        if False and exit_code != 0 and self.email_on_error:
            message = '''
                Command: %(args_repr)s
                
                Container started: %(container_start)s
                Container ended: %(container_end)s
                Run duration: %(container_run_duration)s
                Exit code: %(exit_code)s
                Error: %(stderr)s
            ''' % locals()
            self.send_email(subject='Container error', message=message)
        
        if exit_afterwards:
            if self.email_on_completion:
                message = '''
                    Command: %(args_repr)s
                    
                    Container started: %(container_start)s
                    Container ended: %(container_end)s (%(container_run_duration)s)
                    Exit code: %(exit_code)s
                ''' % locals()
                self.send_email(subject='Analysis completed', message=message)
            exit(exit_code)
        elif exit_code != 0:
            raise Exception('Docker command returned non-zero exit code: %d' % exit_code)
        
        return process

    def send_email(self, recipients=('self',), subject='no subject', message=''):
        try:
            assert message, 'No email message supplied'
            recipients = [r if r != 'self' else self.username for r in recipients]
            assert recipients, 'No email recipients specified'
            
            user_mapper = {
                'amay': ('Ali', 'ali.may@baseclear.com'),
                'ikolder': ('Iris', 'iris.kolder@baseclear.com'),
                'mterhaar': ('Marta', 'marta.terhaar@baseclear.com'),
                'wpirovano': ('Walter', 'walter.pirovano@baseclear.com'),
                'jdenboer': ('Jonathan', 'Jonathan.denBoer@baseclear.com'),
                'bversteeg': ('Benjamin', 'benjamin.versteeg@baseclear.com'),
            }
            if not all(r in user_mapper for r in recipients):
                print(
                    'Not all recipients [%s] were known' % ','.join(map(str, recipients))
                )
                
            from_address = 'SMILE-server <smile@baseclear.nl>'
            recipients_emails = [
                '%s <%s>' % user_mapper[r]
                for r in recipients
            ]
            
            mime_message = MIMEText(message)
            mime_message['Subject'] = subject
            mime_message['From'] = from_address
            mime_message['To'] = ', '.join(recipients_emails)
            
            server = smtplib.SMTP('localhost')
            server.sendmail(from_address, recipients_emails, mime_message.as_string())
            server.quit()
            
        except Exception as error:
            print("Exception occured durring email sending: %s" % error)
    
if __name__ == '__main__':
    try:
        Handler()
    except KeyboardInterrupt:
        pass