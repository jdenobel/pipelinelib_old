from __future__ import print_function    
import functools
import uuid
import os
import subprocess
import warnings
import sys
import json
import pickle
import collections
import socket


# To do:
#  - Delete temp files after completion
#  - DinD possibility; call functions (i.e. start container) from within other functions/containers
#  - arbitrary function serialization; not restricted to class level
#  - dynamic class attributes; realtime updating variables between containers, without going corrupt


DEVNULL = open(os.devnull, 'w')
IS_IN_CONTAINER = len(sys.argv) > 2 and sys.argv[1] == 'EasyDocker'
EASYDOCKER_DIR = os.path.dirname(os.path.realpath(__file__))

LOCAL_TEMP_DIR = os.path.join(EASYDOCKER_DIR, 'tmp')
PROCESS_TEMP_DIR = os.path.join(LOCAL_TEMP_DIR, '%d@%s' % (os.getpid(), socket.gethostname()))
LOCAL_INPUT_DIR = os.path.join(PROCESS_TEMP_DIR, 'container_inputs')
LOCAL_OUTPUT_DIR = os.path.join(PROCESS_TEMP_DIR, 'container_outputs')
os.makedirs(LOCAL_INPUT_DIR)
os.makedirs(LOCAL_OUTPUT_DIR)

CONTAINER_ARGS_KWARGS_PATH = '/mnt/EasyDocker/args_kwargs.pickle'
CONTAINER_OUTPUT_PATH = '/mnt/EasyDocker/output.pickle'
IMAGE_REPOSITORY = 'fs02.compute.local:5000'


class EasyDocker(object):
    def __init__(self, static_attrs=(), **attributes):
        self.__dict__.update(attributes)
        self.__static_attrs = static_attrs
        self.verbose = False
        
        if IS_IN_CONTAINER:
            method = getattr(self, sys.argv[2], None)
            with open(CONTAINER_ARGS_KWARGS_PATH, 'rb') as f_in:
                args, kwargs = pickle.load(f_in)
            if not method or not callable(method):
                raise NotImplementedError('The method "%s" was not implemented or not callable.' % method)
            output = method(*args, execute_local=True, **kwargs)
            with open(CONTAINER_OUTPUT_PATH, 'wb') as f_out:
                pickle.dump(output, f_out)
        else:
            self.wrapper()
        
    @staticmethod
    def check_image_exists(image_name):
        if not hasattr(EasyDocker, '__present_images'):
            #EasyDocker.__present_images = set(
            #    subprocess.check_output([
            #        'docker', '-H', ':4000', 'images', '--format={{.Repository}}'
            #    ]).strip().split()
            #)
            repos = json.loads(
                subprocess.check_output(['curl', '--silent', '%s/v2/_catalog' % IMAGE_REPOSITORY])
            )['repositories']
            EasyDocker.__present_images = frozenset(repos) | frozenset(
                '%s/%s' % (IMAGE_REPOSITORY, r)
                for r in repos
            )
            
        if image_name not in EasyDocker.__present_images:
            warnings.warn('Image "%s" was not found in the swarm\'s repository!' % image_name)
    
    def run_docker_container(self, *args, **kwargs):
        image_name = kwargs.pop('image_name')
        if not image_name.startswith(IMAGE_REPOSITORY):
            image_name = '%s/%s' % (IMAGE_REPOSITORY, image_name)
            
        func = kwargs.pop('func')
        extra_volumes = kwargs.pop('extra_volumes')
        verbose = kwargs.pop('verbose', self.verbose)
        no_stdout = kwargs.pop('no_stdout')
        
        unique_id = str(uuid.uuid4())
        script_file_path = os.path.realpath(func.func_globals['__file__'])
        script_dir_path = os.path.dirname(script_file_path)
        args_kwargs_file_path = os.path.join(LOCAL_INPUT_DIR, unique_id)
        output_file_path = os.path.join(LOCAL_OUTPUT_DIR, unique_id)
        
        volumes = []
        #for v in [
        #    '%s:%s:ro' % (script_dir_path, script_dir_path),
        #    '%s:%s' % (LOCAL_TEMP_DIR, LOCAL_TEMP_DIR),
        #    '%s:%s' % (args_kwargs_file_path, CONTAINER_ARGS_KWARGS_PATH),
        #    '%s:%s' % (output_file_path, CONTAINER_OUTPUT_PATH),
        #] + list(extra_volumes):
        #    if v == '-v':
        #        continue
        #    volumes.extend(['-v', v])
        
        for mount in [
            (script_dir_path,),
            (LOCAL_TEMP_DIR, LOCAL_TEMP_DIR, 'rw'),
            (args_kwargs_file_path, CONTAINER_ARGS_KWARGS_PATH, 'rw'),
            (output_file_path, CONTAINER_OUTPUT_PATH, 'rw'),
        ] + list(extra_volumes):
            if mount == '--mount':
                continue
            if isinstance(mount, tuple):
                assert len(mount) in (1, 2, 3)
                if len(mount) == 3:
                    if mount[-1] == 'ro':
                        ro_mode = 1
                    elif mount[-1] == 'rw':
                        ro_mode = 0
                    else:
                        raise Exception("A volume should be composed as followed: (source[, destination[ ,ro/rw]]) where ro is default.")
                else:
                    ro_mode = 1
                source = mount[0]
                destination = mount[-2] if len(mount) > 1 else source
                mount = 'type=bind,src=%s,dst=%s,ro=%d' % (source, destination, ro_mode)
                
            volumes.extend(['--mount', mount])
        
        with open(args_kwargs_file_path, 'wb') as f_out:
            pickle.dump((args, kwargs), f_out)
        with open(output_file_path, 'w') as f_out:
            pass
                
        cmd = [
            #'docker', '-H', ':4000', 'run', '-it',
            #'/mnt/nfs/docker_utils/docker_helper/main.py', 'service', 'create', 'easy',# '--verbose'
            os.path.join(EASYDOCKER_DIR, '..', 'main.py'), 'service', 'create', 'easy',
        ] + volumes + [
            '-w', script_dir_path,
            #'--entrypoint=python',
            'image=%s' % image_name,
            'python', script_file_path, 'EasyDocker', func.__name__, unique_id
        ]
        if verbose:
            print(' '.join(cmd))
        proc = subprocess.Popen(
            cmd, 
            #stdout=subprocess.PIPE,# if no_stdout else sys.stdout),
            stdin=subprocess.PIPE,# if no_stdout else sys.stdin)
        )
        def load_output():
            with open(output_file_path, 'rb') as f_in:
                return pickle.load(f_in)
        return proc, load_output
        
def dockerize(image_name, wait=True, verbose=None, parallel=False, unwrap_arguments=True, volumes=()):
    def outer_wrapper(func):
        if not IS_IN_CONTAINER:
            EasyDocker.check_image_exists(image_name)
            
        @functools.wraps(func)
        def inner_wrapper(manager, *args, **kwargs):
            assert isinstance(manager, EasyDocker)
            
            extra_volumes = list(kwargs.pop('extra_volumes', []))
            extra_volumes.extend(list(volumes))
            
            if kwargs.pop('execute_local', False):
                return func(manager, *args, **kwargs)
            
            run = functools.partial(
                manager.run_docker_container, 
                image_name=image_name, func=func, extra_volumes=extra_volumes, verbose=verbose,
                no_stdout=parallel
            )
            if parallel:
                if len(args) > 1 or not isinstance(args[0], collections.Iterable) or kwargs:
                    raise Exception('If parallel is true, then only first positional argument (must be iterable) is accepted.')
                
                if unwrap_arguments:
                    containers = [
                        run(**a) if isinstance(a, dict) \
                        else run(*a) if isinstance(a, (tuple, list)) \
                        else run(a)
                        for a in args[0] 
                    ]
                else:
                    containers = [
                        run(a) for a in args 
                    ]
                
                if not wait:
                    return containers
                output_list = []
                for proc, output_loader in containers:
                    assert proc.wait() == 0, "Error occurred in container."
                    output_list.append( output_loader() )
                return output_list
                
            else:
                proc, load_output = run(*args, **kwargs)
                if wait:
                    assert proc.wait() == 0, "Error occurred in container."
                    return load_output()
                return proc, load_output
                
        return inner_wrapper
    return outer_wrapper