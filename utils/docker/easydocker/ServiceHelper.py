from __future__ import print_function
import subprocess
import random
import json
import re
import time
import atexit
import functools
import os
import sys


DEVNULL = open(os.devnull, 'w')
DOCKER_HELPER = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'main.py'
)

class DockerException(Exception): pass


class ServiceHelper(object):
    def __init__(self, image_name, extra_create_args=(), auto_remove=True, verbose=False, dont_mount_passwd=False):
        assert image_name, 'Image name was not supplied'
        self.image_name = image_name
        self.auto_remove = auto_remove
        self.verbose = verbose
        self.dont_mount_passwd = dont_mount_passwd
        
        self.service_ps = None
        self.service_was_removed = False
        self.service_id = None
        
        if auto_remove:
            atexit.register(
                functools.partial(
                    self.remove_service,
                    fail_silently=True
                )
            )
        self.start_service(extra_create_args)
    
    def echo(self, *args, **kwargs):
        if self.verbose:
            print(*args, **kwargs)
    
    def __del__(self):
        if getattr(self, 'auto_remove', True):
            self.remove_service(fail_silently=True)
    
    def start_service(self, extra_create_args=()):
        extra_create_args = list(extra_create_args)
        if self.dont_mount_passwd:
            extra_create_args.append('--dont_mount_passwd')
            
        args = [
            DOCKER_HELPER, 'service', 'create', '--detach=true',
            '--restart-condition', 'none',
            '--endpoint-mode', 'vip',
        ] + extra_create_args + [
            self.image_name, 'sleep', 'infinity'
        ]
        self.service_id = self.check_output(args)
        self.echo('Service started:', self.service_id)
    
    def wait_until_running(self):
        self.echo('Waiting for container to be running...')
        while True:
            self.update_service_ps()
            if not self.service_ps:
                self.echo('Service ps is empty')
            else:
                current_status = self.service_ps['CURRENT STATE'].split(' ', 1)[0].lower()
                self.echo('Current container status:', current_status)
                #if current_status not in ('accepted', 'preparing', 'assigned', 'starting'):
                if current_status == 'running' and self.service_ps.get('NODE', None):
                    break
            time.sleep(1)
        
    def execute(self, entry_args=None, extra_exec_args=(), interactive=False, only_print=False):
        if entry_args:
            if isinstance(entry_args, str):
                entry_args = [entry_args]
            else:
                entry_args = list(entry_args)
        else:
            entry_args = ['/bin/bash']
        
        extra_exec_args = list(extra_exec_args) if extra_exec_args else []
        if interactive:
            extra_exec_args.append('-i')
            
        self.wait_until_running()
        node = self.service_ps['NODE'].split('.', 1)[0]
        remote_container_id = self.get_remote_container_id()
        self.echo('Container is running on', node, 'with id', remote_container_id)
        
        cmd = self.join_cmd(
            [
                'ssh', node,
                '-o', 'LogLevel=QUIET',
                'docker', 'exec', '-t',
            ] + list(extra_exec_args) + [remote_container_id] + entry_args
        )
        
        self.echo('Exec cmd:', cmd)
        if not only_print:
            return subprocess.Popen(cmd, shell=True)
    
    def get_remote_container_id(self):
        node = self.service_ps['NODE'].split('.', 1)[0]
        args = [
            'ssh', node,
            'docker', 'ps',
            '--format', '"{{.ID}}\t{{.Names}}"'
        ]
        docker_ps = dict(
            row.split('\t')
            for row in self.check_output(args).split('\n')
        )
        for container_id, container_name in docker_ps.items():
            if self.service_ps['ID'] in container_name:
                return container_id
        raise DockerException('Docker container was not found on %s' % node)
    
    def inspect_service(self):
        cmd = ['docker', 'service', 'inspect', self.service_id]
        return json.loads(self.check_output(cmd))
    
    def update_service_ps(self):
        self.service_ps = self.extract_ps()
        if self.service_ps.get('ERROR', ''):
            raise DockerException(self.service_ps['ERROR'])
    
    def join_cmd(self, *args):
        by_list = (len(args) == 1) and isinstance(args[0], (list, tuple))
        return ' '.join(
            '"%s"' % a if ' ' in a else a
            for a in map(str, args[0] if by_list else args)
        )
    
    def check_output(self, *args, **kwargs):
        self.echo('Exec cmd:', self.join_cmd(*args))
        return subprocess.check_output(*args, **kwargs).strip()
    
    def extract_ps(self, force_single=True):
        row_list = list(map(str.strip,
            self.check_output(['docker', 'service', 'ps', self.service_id]).strip().split('\n')
        ))
        max_len = max(len(r) for r in row_list)
        
        header, row_list = row_list[0].strip(), row_list[1:]

        end_positions = [
            group.end()
            for group in re.finditer(' {2,}', header)
        ] + [max_len]
        column_list = [
            header[start:end].strip()
            for start, end in zip([0] + end_positions[:-1], end_positions)
        ]
        row_list = [
            [
                row[start:end].strip()
                for start, end in zip([0] + end_positions[:-1], end_positions)
            ]
            for row in row_list
        ]
        ps_list = [
            {
                column: value
                for column, value in zip(column_list, row)
            }
            for row in row_list
        ]
        self.echo(ps_list)
        
        if force_single:
            if len(ps_list) == 0:
                return {}
            assert len(ps_list) == 1
            return ps_list[0]
        return ps_list
        
    def remove_service(self, fail_silently=False):
        if not hasattr(self, 'service_id') or self.service_id is None or self.service_was_removed:
            return
        self.echo('Removing service...')
        args = ['docker', 'service', 'rm', self.service_id]
        if fail_silently:
            self.echo('Exec cmd:', *args)
            subprocess.call(args, stdout=DEVNULL, stderr=DEVNULL)
            self.service_was_removed = True
        else:
            self.check_output(args)