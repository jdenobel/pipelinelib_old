"""Module that contains helper classes for Docker."""
import os
import sys
import docker
import atexit
import string
import random
import subprocess
import datetime
import threading
from time import sleep
from pprint import pprint

from docker.types import Mount, RestartPolicy, Resources


class DockerException(Exception):
    """"""
    pass


class DockerCommandException(subprocess.CalledProcessError):
    def __init__(self, returncode, cmd, stdout, *args, **kwargs):
        self.extra_message = stdout
        super(DockerCommandException, self).__init__(
            returncode, cmd, stdout,
            *args, **kwargs)

    def __str__(self):
        return (
            f"Command '{self.cmd}' returned non-zero "
            f"exit status {self.returncode} "
            f"Output (with errors):\n {self.extra_message} "
        )



class Service(object):
    """"""
    # TODO: Check if image name exists on swarm
    # TODO: Make restartpolicy optional
    # TODO: Make resources optional
    # TODO: differentiate between private and public methods
    # TODO: instead of ssh, maybe use docker -H :4000 <args>
    #       This can be done in a new client from docker api
    '''
    This class wraps arround a docker service in order to
    handle execution and cleanup. It also adds functionality on
    top of normal docker commands.

    Required arguments:
        * image_name : string

    Optional arguments:
        * mounts : iterable of string in format source:target:<ro/rw>
        * ncpu : int (number of reserved cpu, default 6)
        * auto_remove : boolean, if True, remove docker service after this class is deleted
        * name : string, default=image_name
        * verbose : boolean, if True, log extra output
        * mount_passwd : boolean, if True, /etc/passwd is mounted
        * **kwargs : are passed to docker client.services.create(**kwargs)
            see: https://docker-py.readthedocs.io/en/stable/services.html
            for a list of options

    Important methods:
        * excecute: Executes a docker command on the wrapped services
          returns: a subprocess Popen object.

    '''
    def __init__(self,
                 image_name,
                 client=None,
                 mounts=(),
                 ncpu=6,
                 auto_remove=True,
                 name=None,
                 verbose=False,
                 mount_passwd=True,
                 use_fast_nodes=False,
                 **kwargs
                 ):
        if not image_name:
            # Check if image exists in registry
            raise DockerException('Image name was not supplied')

        self.image_name = image_name
        self.ncpu = ncpu
        self.exception = None
        self.auto_remove = auto_remove
        self.mount_passwd = mount_passwd
        self.extra_mounts = mounts
        self.verbose = verbose
        self.removed = False
        self.name = (name if name else image_name.split("/")[-1])
        self.name = self.name[:55]
        self.name += f"_{Service.id_generator()}"
       

        if not client:
            self.echo("No client provided, using standard client")
            client = docker.from_env()

        self.client = client
        self.status = None
        self.active = False
        self.node = None
        self.container_id = None
        self.use_fast_nodes = use_fast_nodes
        self.nodes = {
            node.attrs.get("ID"): node.attrs.get("Description").get("Hostname")
            for node in self.client.nodes.list()
        }

        if auto_remove:
            atexit.register(self.remove_service)

        self.start_service(**kwargs)

    def start_service(self, **kwargs):
        '''
        Creates the docker service on the swarm.
        '''
        mounts = self.standard_mounts + self.validated_mounts
        self.echo(
            f"Creating service {self.name} with image: {self.image_name}")
        # mounts should be absolute paths otherwise, container creation will
        # fail. if container creation fails, you will see a service being
        # created, but no containers. If this happens you can have an idea if
        # you run docker service ps <id/service_name>.
        #self.echo(f"The following mounts will be used:\n{mounts}.")
        self.service = self.client.services.create(
            self.image_name,
            user=self.user,
            name=self.name,
            restart_policy=self.restart_policy,
            resources=self.resources,
            mounts=mounts,
            command="sleep 100000000",
            constraints=["node.labels.node == novaseq"] if self.use_fast_nodes else None,
            **kwargs
        )

    def execute(self,
                command=None,
                cleanup=True,
                extra_exec_args=(),
                interactive=False,
                **kwargs):
        '''
        Executes a docker command on the running service.

        Arguments:
            * command : string, e.g. 'python3 main.py' or /bin/bash,
            * extra_exec_args : iterable  of extra docker exec args
            * interactive : boolean, if the command must be interactive
            * kwargs : passed into subprocess.Popen
                see: https://docs.python.org/3.7/library/subprocess.html
                for a list of options
        Returns:
            A suprocess.Popen object
        '''

        if command:
            if isinstance(command, str):
                command = [command]
            else:
                command = list(command)
        else:
            command = ['/bin/bash']

        extra_exec_args = list(extra_exec_args) if extra_exec_args else []
        if interactive:
            extra_exec_args.append('-i')

        self.wait_until_running()
        self.echo(f"Executing command: {command} in container: "
                  f"{self.remote_container_id} on node {self.node}")
        cmd = self.join_cmd([
            'ssh', self.node,
            # '-o', 'LogLevel=QUIET',
            'docker', 'exec', '-t',
            ] + list(extra_exec_args) + [self.remote_container_id] + command
        )
        popen_obj = subprocess.Popen(cmd, shell=True, stderr=subprocess.STDOUT, **kwargs)

        if cleanup:
            Service.run_and_remove(self, popen_obj)
        return popen_obj


    def execute_and_wait(self, *args, **kwargs):
        p = Service.wait_for_p(
                self.execute(*args, cleanup=False, **kwargs)
        )
        self.remove_service()
        return p

    @staticmethod
    def wait_for_p(p):
        p.wait()
        if p.returncode != 0:
            raise DockerCommandException(
                p.returncode, p.args, p.stdout)
        return p

    @staticmethod
    def wait_for_services(services=()):
        '''
        Tuple: (service, process)
        '''
        return [Service.wait_for_p(p) for service, p in services]


    @staticmethod
    def run_and_remove(service, process):
        def in_thread(service, process):
            process.wait()
            service.remove_service()
        threading.Thread(target=in_thread, args=(service, process)).start()


    @property
    def validated_mounts(self):
        mounts = []
        for vol in self.extra_mounts:
            splitted = vol.split(":")
            assert len(splitted) == 3, \
                f"{vol} not in correct format of source:target:options"
            assert os.path.isdir(splitted[0]), \
                f"Source dir {splitted[0]}, does not exist"
            assert splitted[2] in ["ro", "rw"], \
                f"Mount option {splitted[2]} is invalid"
            mounts.append(
                Mount(
                    source=splitted[0],
                    target=splitted[1],
                    type='bind',
                    read_only=splitted[2] == 'ro' or splitted[2] == 'readonly'
                )
            )
        return mounts

    def echo(self, msg, *args, **kwargs):
        if self.verbose:
            if type(msg) == str:
                msg = f"[{datetime.datetime.now()}] {msg}"
                print(msg, *args, **kwargs)
            else:
                pprint(msg, *args, **kwargs)

    @staticmethod
    def build_mount(source, target, mtype='bind', read_only=False, **kwargs):
        return Mount(
            source=source,
            target=target,
            type=mtype,
            read_only=read_only,
            **kwargs
        )

    @staticmethod
    def id_generator(size=4, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

    @property
    def restart_policy(self):
        return RestartPolicy(condition='none')

    @property
    def resources(self):
        return Resources(cpu_reservation=int(self.ncpu * (10 **9)))

    @property
    def user(self):
        return f"{os.getuid()}:100"

    def get_remote_container_id(self):
        if self.container_id:
            remote_ps = dict(
                row.split("\t") for row in subprocess.check_output([
                    'ssh', self.node,
                    'docker', 'ps',
                    '--format', '"{{.ID}}\t{{.Names}}"'
                    ]).strip().decode().splitlines())

            for container_id, container_name in remote_ps.items():
                if self.container_id in container_name:
                    return container_id

        raise DockerException()

    @property
    def remote_container_id(self):
        '''
        This seems realy redundant, but sometimes, there is tiny
        delay between a the creation and acceptance of a service.
        '''
        for t in range(9):
            try:
                return self.get_remote_container_id()
            except DockerException:
                sleep(2**t)
                continue

        self.service.reload()
        task_list = self.service.tasks()
        task_dict = task_list[0]
        raise DockerException(
            (
                f'Docker {self.name} was not found on {self.node} '
                f'(This usually only happens when the server is really busy)\n'
                f"{str(task_dict)}"
            )
        )

    @property
    def standard_mounts(self):
        '''
        Standard mounts are:
            * /etc/group
            * /etc/localtime
            * user home dir

        Optionally /etc/passwd is added
        '''
        home_dir = os.path.expanduser('~')
        return [
            Service.build_mount('/etc/group', '/etc/group', read_only=True),
            Service.build_mount('/etc/localtime', '/etc/localtime', read_only=True),
            Service.build_mount(home_dir, home_dir),
        ] + ([
            Service.build_mount('/etc/passwd', '/etc/passwd', read_only=True),
        ] if self.mount_passwd else [])

    def poll(self):
        self.service.reload()
        task_list = self.service.tasks()
        if len(task_list) == 0:
            return

        task_dict = task_list[0]
        self.active = task_dict.get("DesiredState") == "running"
        self.node = self.nodes.get(task_dict.get("NodeID"))
        self.container_id = task_dict.get("ID")
        self.status = task_dict.get("Status")
        message = self.status.get("Err") or self.status.get("Message")
        # TODO: Check if docker has failed here, hard exit
        if self.verbose:
            sys.stdout.write(f"\r[{datetime.datetime.now()}] Status: {message}" )
            sys.stdout.flush()

    def wait_until_running(self):
        self.poll()
        while not self.active or not self.node or not self.container_id:
            self.poll()
        if self.verbose:
            sys.stdout.write("\n")
        self.node = self.node.split(".")[0]

    def join_cmd(self, *args):
        by_list = (len(args) == 1) and isinstance(args[0], (list, tuple))
        return ' '.join(
            '"%s"' % a if ' ' in a else a
            for a in map(str, args[0] if by_list else args)
        )

    def remove_service(self, t=0):
        '''
        Tries to remove a service 3 times,
        raises APIError
        '''
        if not self.removed:
            try:
                self.service.remove()
                self.echo(f"Removed service {self.name}")
                self.removed = True
            except docker.errors.APIError as err:
                sleep(3)
                self.remove_service(t=t+1)
                if t == 3:
                    raise err

    def __repr__(self):
        return f"<{type(self).__name__}: {self.name}>"

    def __del__(self, *args, **kwargs):
        if self.auto_remove:
            self.remove_service()


if __name__ == "__main__":
    test = Service(
        image_name="ubuntu",
        mount_passwd=False,
        # ncpu=500,
        mounts=("/mnt/nfs/pipelines/pipelineliba:/pipelinelib:ro",),
        workdir="/pipelinelib"
    )
    # test is a docker service
    import pdb; pdb.set_trace()
    # spawn multiple commands
    # processes = [
    #     test.execute('sleep %s' % number, stdout=open("logs.txt", "a+"))
    #     for number in range(5)
    # ]
    # # wait for all commands to complete
    # for p in processes:
    #     p.wait()
    #     print(p.returncode)
