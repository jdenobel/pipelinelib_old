User Code
=========

Here all the pipelines that use the Library are explained.

Demultiplexing
--------------
.. automodule:: illumina_demultiplexing
   :members:

CPA Assembly
------------
.. automodule:: cpa_assembly
   :members:
