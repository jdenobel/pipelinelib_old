library
=======
These are classes and methods that have generic functionality.
.. automodule:: utils.file

RunInfo
-------

.. autoclass:: utils.file.RunInfo
  :members:

Bcl2FastqSamplesheet
--------------------

.. autoclass:: utils.file.bcl2fastqsamplesheet
  :members:

Excel
-----

.. autoclass:: utils.file.Excel
  :members:
.. autoclass:: utils.file.F088
  :members:
.. autoclass:: utils.file.F089
  :members:

FTPHelper
---------

.. autoclass:: utils.file.ImplicitFTP_TLS
.. autoclass:: utils.file.FTPHelper

LaneBarcode
-----------

.. autoclass:: utils.file.LaneBarcode

FileMover
---------

.. autoclass:: utils.file.FileMover


Exceptions
----------

.. autoclass:: utils.file.DuplicateDataError
  :members: __init__
.. autoclass:: utils.file.NoSampleForPoolException
  :members: __init__


.. automodule:: utils.docker

Service
-------
.. autoclass:: utils.docker.Service
  :members:
