Welcome to Pipeline Library's documentation!
============================================
The library was created in order to abstract some of the common functionality
that is available through all pipelines. The following documentation contains
detail information about the implementation of the library as well as its
possible uses.

In this document we make a distinction between library and user code. Library
code is intended to be reusable and very generic whereas user code is for
very specific situations. In case of special situations it is advised to create
new scripts using the generic script as a base for the "special situation".
*never hard code client names, directory paths, etc on the user code!*. Use
environment variables instead.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   contents/user/index
   contents/library/index

Running Pipelines
-----------------
On the smile server, under the pipeline directory, the master branch is cloned.
However, if by any reason, you need to debug, it is recommended to clone the
repository in another location in the smile server (preferably a users's home
directory). After the correct repository has been cloned, then you can proceed
to make changes. This ensures that if someone else happens to use the pipeline,
they don't use your changes.

Be aware that by cloning to your own repository, you may produce permission
denied errors. If this happens, the easiest solution is to change permissions
for group in the run folder where you are testing/developing. Moreover, if while
using the Docker service class (`utils.Service`) you are asked for passwords for
a node, then that means that the your user doesn't have his ssh-key setup on the
nodes. In this situation, is advised to use the production user.

Demultiplexing Pipeline
-----------------------
A high level reimplementation of the Demultiplexing pipeline can be seen in
:numref:`figure %s <figure-demultiplexing-overview>`.

.. _figure-demultiplexing-overview:
.. figure:: figures/demultiplexing_overview.png
    :align: center

The process starts right after a MySeq, HiSeq or NovaSeq run is completed. At
this point Bioinformaticians would check if the Sample Sheets provided by the
Lab is accurate. If the Sample Sheets have invalid format, they would need to
be corrected. If they are correct, then tools would be called to perform
analysis on the data that is on a "run folder". The following tools are used:

* Bcl2Fastq
* Check Undetermined Indices
* Trim Adapters
* Phix Filtering
* FastQC

If Any of the tools raise an exception, then the process is stopped

.. todo::

  redirect to the logging library for more information about failures.

After the successful run of the tools, data is moved accordingly to the
respective output locations.

CPA Assembly pipeline
---------------------
Currently is a wrapper over the previous CPA pipeline.
