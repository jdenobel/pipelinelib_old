#!/mnt/nfs/pipelines/pipelinelib/venv/bin/python3
"""Client Code for running Illumina Demultiplexing.

To run in its simplest form you can use:

``python3 illumina_demultiplexing.py <bcl2fastq_samplesheet>``

For extra details see `get_required_arguments` for extra parameters.

If default parameters are used and these parameters refer to files/directories,
then the script will look for this files/directories on the directory where the
script is being called from.

If you have pools, pellets, etc (rows in the bcl2fastq that imply a group of
samples). then you need to pass --additional-sheets argument with the
sample sheet filenames that must be present on the -run_folder. Otherwise,
the script will stop running the pipeline.

The script will call the following dockers:
    * Bcl2fastq
    * Check Undetermined indices
    * Trim Adapter
    * Phix Filtering
    * FastQC
"""
import argparse
import glob
import io
import os
import shutil
import subprocess
import sys
import zipfile

import docker
from interop import py_interop_run_metrics, py_interop_summary
import pandas as pd
import pysftp
from raven import Client
import xmltodict

from utils.file import (
    Bcl2FastqSampleSheet,
    DuplicateDataError,
    FileMover,
    F088,
    F089,
    LaneBarcode,
    Nextera,
    NoSampleForPoolException,
    RunInfo,
)
from utils import send_email, get_project_dir, wait_for
from utils.docker import Service
from utils.logger import Logger


RUN_INFO_XML = 'RunInfo.xml'
DOCKER_CLIENT = docker.from_env()

# TODO: change this to the correct directory.
NGS_FOLDER = os.path.realpath('/mnt/nfs/rawdata/NGS-projects/')
VALIDATION_NGS_FOLDER = os.path.realpath('/mnt/nfs/validatie_sets/W-BI-009_Illumina_demultiplexing/results/NGS_Projects')
#VALIDATION_NGS_FOLDER = os.path.realpath('/mnt/nas/storage/novaseq_test/results/NGS_Projects')

OP_FOLDER = os.path.realpath("/mnt/web01/output_unchecked/")
VALIDATION_OP_FOLDER = os.path.realpath("/mnt/nfs/validatie_sets/W-BI-009_Illumina_demultiplexing/results/output_unchecked")
#VALIDATION_OP_FOLDER = os.path.realpath("/mnt/nas/storage/novaseq_test/results/output_unchecked")

RIVM_VALIDATION_TARGET = os.path.realpath('/mnt/nfs/validatie_sets/W-BI-009_Illumina_demultiplexing/results/RIVM_SFTP')
#RIVM_VALIDATION_TARGET = os.path.realpath('/mnt/nas/storage/novaseq_test/results/RIVM_SFTP')

SEQUENCE_DATA_FOLDER = 'Data/Intensities/BaseCalls/SequenceData/'
VALIDATION_SEQUENCE_DATA_FOLDER = 'Data/Intensities/BaseCalls/ValidationData/'

RIVM_SFTP_HOST = 'sftp.rivm.nl'
VALIDATION_RIVM_SFTP_HOST = 'control01'
# TODO: Hide this in env var
RIVM_SFTP_USER = 'baseclear'
VALIDATION_RIVM_SFTP_USER = 'prod'
RIVM_SFTP_PASSWORD = 'B4s3Cl34r'
VALIDATION_RIVM_SFTP_PASSWORD = 'illumina'

WATCHER_EMAILS = [
    "marco.antonleon@baseclear.nl",
    "hilde.stawski@baseclear.nl",
    "marta.terhaar@baseclear.nl",
    "mark.bessem@baseclear.nl",
    "mirna.baak@baseclear.nl"
]
ERROR_CLIENT = Client("https://fb2f365fbf50457fac0c28489148e9e9@sentry.io/1314510")


def get_required_arguments():
    """Get the required arguments for demultiplexing.
    Will look for the following arguments:

    * bcl2fastq_samplesheet: absolute path of a samplesheet. Ensure that the
        name of the samplesheet does not contain spaces.
    * run_folder: folder where all the data required for fastqc is located. If
        not given, it will use the `dirname` of the `bcl2fastq_samplesheet`
        argument.
    * output_folder: relative path where the temporary files will be stored and
        location where the clean up will remove temporary files. It defaults to
        `/Data/Intensities/BaseCalls/SequenceData`
    * mismatches
    * adapter-length
    * extra_opts
    * copy-to-op
    * wait
    * rivm-projects
    * additional-sheets
    * merge-dirs
    * filters

    Additionaly it will create  a log directory (`pipeline_logs`) in the run
    folder. See the --help flag for more details.

    Returns: A namespace containing the above values.
    """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "bcl2fastq_samplesheet",
        help="Absolute path to a sample sheet file. Samplesheets are usually "
             "located in the run folder. "
        )
    parser.add_argument(
        '-f',
        "--run_folder",
        default=None,
        help="Location where the script will find all the required "
             "data. If not given, it will use the dirname of the "
             "`bcl2fastq_samplesheet`"
        )
    parser.add_argument(
        '-o',
        "--output_folder",
        default=SEQUENCE_DATA_FOLDER,
        help="Output folder. This is a relative path to the `run-folder`. "
             "This is the working directory for the pipeline."
        )
    parser.add_argument(
        "-m",
        '--mismatches',
        default=1,
        help='Number of mismatches allowed in barcode (default: 1).'
        )
    parser.add_argument(
        "-l",
        '--adapter-length',
        default=50,
        help='Length of the adapter default: 50).'
        )
    parser.add_argument(
        "-e",
        '--extra_opts',
        default=None,
        help='Extra options for bcl2fastq2, in quotes! '
             'e.g. "--ignore-missing-bcl".',
        )
    parser.add_argument(
        "-p",
        '--copy-to-op',
        action='store_true',
        help='(OPTIONAL) If True, the compressed raw data will be '
             'automatically transferred to Order Portal.'
             'This option includes the generated md5sum file (edited without '
             'BC_blancos) and xml reports.'
        )
    parser.add_argument(
        "-w",
        '--wait',
        action='store_true',
        help='(OPTIONAL) If True, wait for data to be sequenced (wait for RTA '
             'complete + half hour)'
        )
    parser.add_argument(
        "--rivm-projects",
        nargs="*",
        help="List of projects that should be uploaded to the rivm ftp server."
        )
    parser.add_argument(
        "--additional-sheets",
        nargs="*",
        help="If in the bcl2fastq sample sheets we have pools, pellets, etc."
             "provide the samplesheets filenames that contain this info. "
             "For F088, you must provide the plate id from ET. For FO89 and "
             "Nextera, you have to provide the index tag (A, B, C or D)."
        )
    parser.add_argument(
        "--merge-dirs",
        nargs="*",
        help="Comma separated key-value pairs denoting projects and merging "
             "directorys. For example: `... "
             "--merge-dirs <project> <folder>,"
        )
    parser.add_argument(
        "--filters", nargs="*",
        help="Comma separated key-value pairs denoting projects and amount "
             "of data in MB required by customer. For example: `... "
             "--filters <project> <data>, <project> <data>, ...`. If all "
             "projects have the same `<data>` requirement then provide one "
             "value. For example: `--filters <data>`."
        )
    parser.add_argument(
        "--validation",
        action='store_true',
        help="pass this flag when you are running validation to use different "
             "default folders to write data to."
        )

    args = parser.parse_args()
    if not args.run_folder:
        setattr(args, "run_folder", os.path.dirname(args.bcl2fastq_samplesheet))

    # change default folders to validation folders.
    if args.validation and args.output_folder is SEQUENCE_DATA_FOLDER:
        setattr(args, "output_folder", VALIDATION_SEQUENCE_DATA_FOLDER)

    # Setting  & normalizing usefull paths
    setattr(args, "output_folder", os.path.normpath(args.output_folder))
    # Ensuring that run_folder is abs path (mainly for docker mounts).
    setattr(args, "run_folder", os.path.abspath(args.run_folder))

    datadir = (
        os.path.join(
            args.run_folder,
            os.path.normpath(args.output_folder)
        )
        if args.run_folder not in args.output_folder
        else args.output_folder
    )
    setattr(args, "abs_output_folder", datadir)
    setattr(args, "output_dir_basename", os.path.basename(
        os.path.normpath(args.output_folder)
    ))
    log_dir = os.path.join(args.run_folder, "pipeline_logs")
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)
    setattr(args, "log_dir", log_dir)
    return args


def generate_samplesheets(arguments, runinfo=RUN_INFO_XML, **kwargs):
    """Generate samplesheets.

    Will generate a list of samplesheets. depending on the amount of indeces
    found in the data provided by additional samplesheets and original
    samplesheet.
    """
    can_write = kwargs.get('can_write', True)
    runinfo_path = os.path.join(arguments.run_folder, runinfo)
    # parse_run_data will raise exceptions...
    run_info, samplesheet = parse_run_data(runinfo_path, arguments)
    if can_write:
        sss_full_path = samplesheet.write(run_info)
        ss_filenames = [os.path.basename(ss) for ss in sss_full_path]
    return samplesheet, run_info, ss_filenames


def is_run_completed(arguments, **kwargs):
    """Check if the has been completed.

    Essentially checks for a file in the `run_folder` that is named
    RTAComplete.txt. Also calls wait_for, if wait is true in c.l.a.

    Arguments:
        * arguments (`Namespace`): arguments passed to the cli. it will look
            in the run folder for 'RTAComplete.txt'

    Returns: True if checks are successful, false otherwise.
    """
    rta_file = os.path.join(arguments.run_folder, 'RTAComplete.txt')
    if kwargs.get('generate_ss', True):
        generate_samplesheets(arguments)
    if arguments.wait and not os.path.isfile(rta_file):
        wait_for(rta_file)
    return os.path.isfile(rta_file)


def parse_run_data(run_info_path, arguments):
    """Parse the data required from lab run.

    Reads through csv and excel sheets to be able to get an object that can
    be used to generate a samplesheet_checked.

    Arguments:
        * run_info_path (`str`): a path to the RunInfo.xml file.
        * arguments (`Namespace`): arguments passed to cli

    Raises:
        * `ValueError` if not enough samplesheets were passed. Amount of
            additional samplesheets is based on the amount of plates found on
            the bcl2fastq_samplesheet.

    Retunrs:
        * RunInfo (:obj:`RunInfo`): object containing the data from RunInfo.xml
        * Bcl2Fastq (:obj:`Bcl2Fastq`): object that contains the data from the
            samplesheets.
    """
    LOGGER.log("Checking Samplesheets")
    run_info = RunInfo(run_info_path)
    try:
        # TODO: check for spaces in the samplesheet.
        samplesheet = Bcl2FastqSampleSheet(arguments.bcl2fastq_samplesheet)
    except DuplicateDataError as e:
        sys.exit(e.message)
    except NoSampleForPoolException as e:
        sys.exit(e.message)

    if samplesheet.requires_additional_data:
        LOGGER.log("Additional samplesheets are required! "
                   "Attempting to parse additional samplesheets.")

        if arguments.additional_sheets and len(arguments.additional_sheets) > 0:
            meta = []
            for i in arguments.additional_sheets:
                meta.append(i)
                if i.endswith("xlsx") or i.endswith("xls"):
                    LOGGER.log(f"Trying to parse: {i}")
                    sheet = os.path.join(arguments.run_folder, i)
                    if len(meta) == 2:  # F088
                        samplesheet.update_data(
                            F088(sheet, plate_id=meta[0])
                        )
                    elif len(meta) == 3 and meta[1] in "ABCD":  # F089
                        samplesheet.update_data(
                            F089(sheet, plate_id=meta[0], index_set_id=meta[1])
                        )
                    elif len(meta) == 3:
                        # Check for custom file is provided
                        custom_tags = os.path.join(arguments.run_folder,
                                                   os.path.basename(meta[1]))
                        custom_tags = custom_tags if\
                            os.path.isfile(custom_tags)\
                            else None
                        samplesheet.update_data(
                            Nextera(sheet, plate_id=meta[0],
                                    custom_tags=custom_tags)
                        )
                    meta = []

        if samplesheet.requires_additional_data:
            raise ValueError((
                f"More samplesheets are required! Still "
                f"missing {len(samplesheet.unseen_plates)} plate(s)"
                ))
    if run_info.is_sr:
        samplesheet.data['Index2'] = ""
        samplesheet.data['Index'] = samplesheet.data['Index'].str.slice(0, 6)

    return (run_info, samplesheet)


def bcl2fastq(run_folder, runinfo, samplesheet_dict, is_novaseq, **kwargs):
    """"""
    ncpu = kwargs.get("ncpu", 20)
    extra_opts = kwargs.get("extra_opts", None)
    mismatches = kwargs.get("mismatches", None)
    logdir = kwargs.get('logdir', '')

    services = []
    log_files = []
    index_pairs = []
    image = ('fs02.compute.local:5000/baseclear/bcl2fastq2:2.20'
             if is_novaseq else
             'fs02.compute.local:5000/baseclear/bcl2fastq2.18:1.0')

    for samplesheet, data_folder in samplesheet_dict.items():
        ndx_string = samplesheet.split('_')[-1].split('.')[0]
        # index_string is of the form CHECKED8-8 or CHECKED10-10 or CHECKED10-8
        # need to extrat the digits.
        indices = ndx_string.split('-')
        index1 = indices[0][-2:] if indices[0][-2:].isdigit() else indices[0][-1:]
        index2 = indices[1]
        indices = (index1, index2)
        index_pairs.append(indices)
        basesmask = f"--use-bases-mask Y*,I{indices[0]}n*,I{indices[1]}n*,Y*"
        service_name = f"bcl2fastq2_{runinfo.flowcell_id}_{ndx_string}"
        command = (f"bcl2fastq --runfolder-dir /runfolder "
                   f"--output-dir /runfolder/{data_folder} "
                   f"--sample-sheet /runfolder/{samplesheet} "
                   f"{'--with-failed-reads ' if not is_novaseq else ' '} "
                   f"{extra_opts if extra_opts else ' '}"
                   f"--barcode-mismatches {mismatches} "
                   f" --no-lane-splitting {basesmask}")
        service = Service(image_name=image,
                          client=DOCKER_CLIENT,
                          name=service_name,
                          mounts=(f"{run_folder}:/runfolder:rw",),
                          ncpu=ncpu,
                          use_fast_nodes=is_novaseq)
        # WARNING: Opening log files...
        log_file = open(os.path.join(logdir, f"{service_name}.txt"), "w+")
        log_files.append(log_file)
        # TODO: does not write to the log file...
        LOGGER.log(f"Running bcl2fastq under {service_name}.")
        services.append((service, service.execute(command, stdout=log_file)))
    Service.wait_for_services(services)
    [file.close() for file in log_files]
    return index_pairs


def check_undetermined_and_stats(run_folder, data_folders, runinfo, samplesheet, **kwargs):
    """Runs `getIndexesFromUndetermined.pl` and `getRunStats.py` scripts.

    The scripts are in undetermined_check:1.0 docker. It will output its data to
    Undetermined_indices file. Will generate the RUN_SUMMARY_SAV.

    Arguments:
        * args (`Namespace`): a namespace that has output_folder,
            output_dir_basename and run_folder.
        * runinfo (`RunInfo`): run info object generated from  RunInfo.xml
        * samplesheet (`Bcl2FastqSampleSheet`): a samplesheet used for the run.
    """
    logdir = kwargs.get('logdir', '')
    is_novaseq = samplesheet.is_novaseq
    is_validation = kwargs.get('is_validation', True)
    ncpu = kwargs.get("ncpu", 1)
    script = kwargs.get('script', '/scripts/getIndexesFromUndetermined.pl')
    image = kwargs.get('image', 'fs02.compute.local:5000/baseclear/undetermined_check:1.0')

    LOGGER.log(f"Running check undetermined and stats.")
    services = []
    log_files = []

    # This statistics are run dependent and not different folders
    get_run_statistics(run_folder,
                       runinfo,
                       "run_statistics.xml",
                       write_summaries=is_validation)

    for folder in data_folders:
        result_filename = os.path.basename(os.path.normpath(folder))
        service_name = f'undetermined_{result_filename}'
        command = (f"/bin/bash -c 'perl {script} "
                   f"/runfolder/{folder}/Undetermined_*.gz "
                   f"> /runfolder/Undetermined_indices_"
                   f"{result_filename}.txt'")
        service = Service(image_name=image,
                          client=DOCKER_CLIENT,
                          name=service_name,
                          mounts=(f"{run_folder}:/runfolder:rw",),
                          ncpu=ncpu,
                          use_fast_nodes=is_novaseq)
        logfile = open(os.path.join(logdir, f"{service_name}.txt"), "w+")
        log_files.append(logfile)
        services.append((service, service.execute(command, stdout=logfile)))
    Service.wait_for_services(services)
    [file.close() for file in log_files]
    process_undet_indices(run_folder, samplesheet)

def process_undet_indices(run_folder, ss):
    if ss.has_index_tags_combinations:
        search_tags = ss.data[
            ss.data['IndexLength'] == 10][['Index', 'Index2']]\
            .apply(lambda x: x.str.slice(0, 8), axis=1)\
            .apply(lambda x: ''.join(x), axis=1)
        try:
            undet_file = glob.glob(os.path.join(
                run_folder, "Undetermined_indices_*8-8.txt"))[0]
        except IndexError:
            raise RuntimeError(
                "Cannot find Undetermined_indices_*8-8.txt in {}".format(
                    run_folder))
        outfile = os.path.join(
                run_folder, "Undetermined_indices_FINAL.txt")
        with open(undet_file, 'r') as infile:
            with open(outfile, "w+") as outfile:
                for line in infile.readlines():
                    if not any(tag in line for tag in search_tags):
                        outfile.write(line)



def create_dataframe(columns, rows):
    """Create a dataframe based on columns and rows.

    columns is a dictionary that contains the funciton name that would be used
    to get the values from the rows.
    Arguments:
        * columns (obj:`dict`): A dictionary that contains a list of column
            labels (keys) mapped to a string representing a function.
        * rows (obj:`dict`): A dictionary that has its keys an index and is
            mapped to an object from which each the value of columns can be
            called.
    Returns:
        A dataframe combining the data from columns and rows.
    """
    index = rows.keys()
    pandas_dictionary = {}
    for column_name, function in columns.items():
        values = []
        for label, object in rows.items():
            a_function = getattr(object, function)
            if hasattr(a_function(), 'mean'):
                values.append(a_function().mean())
            else:
                values.append(a_function())
        pandas_dictionary[column_name] = pd.Series(values, index)
    return pd.DataFrame(pandas_dictionary)


def create_general_summary(summary):
    """Creates general run statistics dataframe."""
    column_map = {
        'Yield Total (G)': 'yield_g',
        'Projected Yield': 'projected_yield_g',
        '% Aligned': 'percent_aligned',
        'Error Rate': 'error_rate',
        'Intensity Cycle 1': 'first_cycle_intensity',
        '% >= Q30': 'percent_gt_q30',
        }
    rows = {
        f'Read {summary.at(index).read().number()}':
            summary.at(index).summary() for index in range(summary.size())
    }
    rows['Non-Indexed Total'] = summary.nonindex_summary()
    rows['Total'] = summary.total_summary()
    return create_dataframe(column_map, rows)


def create_read_summary(summary):
    """"""
    column_map = {
        "Lane": 'lane',
        'Tiles': 'tile_count',
        'Density': 'density',
        '% Clusters PF': 'percent_pf',
        'Reads': 'reads',
        'Reads PF': 'reads_pf',
        '% >= Q30': 'percent_gt_q30',
        'Yield (G)': 'yield_g',
        '% Aligned': 'percent_aligned',
        '% Error Rate': 'error_rate',
        'Intensity Cycle 1': 'first_cycle_intensity',
    }
    # could do this in dictionary comprehension, but this is more readable.
    # This is being done on the getRunStatistics script...
    rows = {}
    for size in range(summary.size()):
        for lane in range(summary.lane_count()):
            rows[f'Read {size + 1}_{lane + 1}'] = summary.at(size).at(lane)
    return create_dataframe(column_map, rows)


def write_run_statistic_xml(runinfo,
                            read_statistics,
                            lane_count,
                            output_dirname,
                            output_filename):
    """"""
    runinfo_reads = [
        (f'Read {read.get("@Number")}', read.get('@NumCycles'))
        for read in runinfo.reads
        if read.get('@IsIndexedRead') == 'N'
        ]
    show_last_read_data = len(runinfo_reads) > 1

    lane_list = []
    for lane in range(lane_count):
        lane_nr = lane + 1
        index = f'Read 1_{lane_nr}'
        tiles = int(read_statistics['Tiles'].at[index])
        density = int(read_statistics['Density'].at[index]/1000)
        clusterpf = round(read_statistics['% Clusters PF'].at[index], 2)
        reads = round(read_statistics['Reads'].at[index]/1000000, 2)  # 10^6
        readspf = round(read_statistics['Reads PF'].at[index]/1000000, 2)
        percent_aligned = round(read_statistics['% Aligned'].at[index], 2)
        cycles_sequenced_read1 = runinfo.reads[0].get('@NumCycles')
        percent_q30_read1 = round(read_statistics['% >= Q30'].at[index], 2)
        error_rate_read1 = round(read_statistics['% Error Rate'].at[index], 2)
        if show_last_read_data:
            last_read = runinfo_reads[-1]
            index_name = f'{last_read[0]}_{lane_nr}'
            cycles_sequenced_last_read = last_read[1]
            perent_q30_last_read = round(read_statistics['% >= Q30'].at[f'{index_name}'], 2)
            error_rate_last_read = round(read_statistics['% Error Rate'].at[f'{index_name}'], 2)

        lane_data = {
            'Laan': lane_nr,
            'Tiles': tiles,
            'Density': density,
            'ClustersPF': clusterpf,
            'Reads': reads,
            'ReadsPF': readspf,
            'AlignedPerc': percent_aligned,
            'CyclesSequencedRead1': cycles_sequenced_read1,
            'PercQ30Read1': percent_q30_read1,
            'ErrorRateRead1': error_rate_read1,
            }
        if show_last_read_data:
            lane_data.update({
                'CyclesSequencedRead2': cycles_sequenced_last_read,
                'PercQ30Read2': perent_q30_last_read,
                'ErrorRateRead2': error_rate_last_read,
                })
        lane_list.append(lane_data)
    summary_report = {
        'RunSummaryReport': {
            'IlluminaID': runinfo.flowcell_id,
            'laneTag': lane_list
            }
        }
    with open(os.path.join(output_dirname, output_filename), "w") as handle:
        handle.write(xmltodict.unparse(summary_report, pretty=True))


def get_run_statistics(run_folder, runinfo, output_file, **kwargs):
    """Get run statistics by using interop tools."""
    run_metrics_obj = py_interop_run_metrics.run_metrics()
    try:
        run_folder_obj = run_metrics_obj.read(run_folder)
    except Exception:
        # sometimes interop raises a invalid_run_info_exception. This seems to
        # be a java exception and not 100% sure if there is a python exeption
        # Seen it happen on *some* test runs. not sure how to reproduce.
        LOGGER.warn(f"Error while trying to generate run_statistics."
                    f"run_metrics_obj raised an exception. Will try to "
                    f"proceed with generation of statistics.")
        # The run_metrics_obj.read performs some opperations for the summary
        # object to be generated?... If it doesnt, then we do not need to call
        # this method and simply continue...
    summary_obj = py_interop_summary.run_summary()
    # is this required? we are performing some work, but not using results?
    # code/documentation from interop does not explain this method properly...
    py_interop_summary.summarize_run_metrics(run_metrics_obj, summary_obj)

    general_statistics = create_general_summary(summary_obj)
    read_statistics = create_read_summary(summary_obj)
    if kwargs.get('write_summaries', None):
        filename = os.path.join(run_folder, 'run_statistics.txt')
        general_statistics.round(2).to_csv(f'{filename}', sep='\t')
        read_statistics.round(2).to_csv(f'{filename}', sep='\t', mode='a')
    write_run_statistic_xml(runinfo,
                            read_statistics,
                            summary_obj.lane_count(),
                            run_folder,
                            output_file)


def rename_file(old_name, sample, underscores=3):
    """Rennaming of a file.

    Preconditions:
        * Expect an extension (.fasta.gz)
    """
    basename, extension = old_name.split(".", maxsplit=1)
    splitted = basename.split("_")
    splitted[-1*underscores] = (f"{sample['Index']}"
                                f"{sample['Index2']}"
                                f"_L00{sample['Lane']}")
    splitted.append(f"{sample['FCID']}.{extension}")
    return "_".join(splitted)


def get_sample_id(filename, samplesheet):
    """Get sample ids based on a their existance on the naming of a file."""
    for sample_id in samplesheet.sample_ids:
        if sample_id in filename:
            return sample_id


def find_files(pattern, samplesheet, verbose=True, rename=True):
    """Tries to find demultiplexed files for all samples in the samplesheet.

    Tries to correct the filenames.
    """
    demultiplexed_files = {
        get_sample_id(filename, samplesheet): [dirname] + filename.split(os.extsep, 1)
        for dirname, filename in [
            os.path.split(path) for path in glob.glob(pattern) or glob.glob("*/*".join(pattern.split("*", 1)))
            ]
        }

    found_samples = samplesheet.data[
        samplesheet.data['Sample_ID'].isin(demultiplexed_files.keys())
        ]

    filt_samples = found_samples[
        found_samples['Recipe'].astype(str) != 'no-filt']

    if len(found_samples) != len(samplesheet.data) and verbose:
        LOGGER.warn(
            f"A different number of samples found on samplesheet"
            f"({len(samplesheet.data)}) than are found as .fastq"
            f" ({len(found_samples)})"
            )

    # Correct file names:
    if rename:
        for index, sample in found_samples.iterrows():
            dirname, filename, extension =\
                demultiplexed_files.get(sample['Sample_ID'])
            if "L00" not in filename:
                file_expression = (f"{filename.replace('_R1_', '_R*_')}"
                                   f".{extension}")
                rfiles = glob.glob(os.path.join(dirname, file_expression))
                # we get R1 and R2 files, now we rename
                for rfile in rfiles:
                    try:
                        new_sample_name = rename_file(
                            os.path.basename(rfile), sample)
                        new_name = os.path.join(dirname, new_sample_name)
                        os.rename(rfile, new_name)
                        # demultiplexed files dict only keeps track of R1.
                        if "_R1_" in new_name:
                            new_name_noext = new_name.split('.', 1)[0]
                            new_name_basename = os.path.basename(new_name_noext)
                            demultiplexed_files[sample['Sample_ID']] =\
                                [dirname, new_name_basename, extension]
                    except OSError as err:
                        LOGGER.warn(
                            f"Couldn't rename {rfile} reason {err}"
                        )

    if verbose:
        LOGGER.log(
            f"Found {len(filt_samples)} samples to be filtered, "
            f"{len(found_samples) - len(filt_samples)} shouldn't be filtered"
        )

    return demultiplexed_files, found_samples, filt_samples


def sample_loop(arguments, samplesheet, samples, demultiplexed_files,
                callback, *args, **kwargs):
    """Execute a 'callback' for the given arguments.

    Arguments:
        * arguments
        * samplesheet
        * samples
        * demultiplexed_files
        * callback
    """
    jobs = []
    files = []
    LOGGER.log(f"Running {callback.__name__}")
    for index, sample in samples.iterrows():
        handle = open(
            os.path.join(arguments.log_dir,
                f"{callback.__name__}_{sample['Sample_ID']}.log"), "w+")
        files.append(handle)

        dirname, r1_base, extension = demultiplexed_files.get(
                                                        sample['Sample_ID'])
        r2_base = r1_base.replace("_R1_", "_R2_")
        r2_base = r2_base if os.path.isfile(
            os.path.join(dirname, f"{r2_base}.{extension}")) else None
        jobs.append(callback(
            sample, dirname,
            r1_base, r2_base, extension, handle, samplesheet,
            *args, **kwargs)
        )
    Service.wait_for_services(jobs)
    [x.close() for x in files]


def trim_adapters(sample, dirname, r1_base, r2_base, extension, stdout,
                  samplesheet, length=50):
    """Run fastq-mcf on a sample.

    fastq-mcf tool is in docker ea-utils1.1.2:1.0

    Arguments:
        * sample
        * dirname
        * r1_base
        * r2_base
        * extension
        * stdout
        * samplesheet
        * length

    """
    # TODO: remove hard coded path or use defaults.
    database = '/mnt/nfs/pipelines/illumina_demultiplexing/adapterseqs'
    service = Service(
        client=DOCKER_CLIENT,
        image_name='fs02.compute.local:5000/baseclear/ea-utils1.1.2:1.0',
        name=f"fastq-mcf_{sample['Sample_Project']}",
        ncpu=6,
        use_fast_nodes=samplesheet.is_novaseq,
        mounts=(
            f"{database}:/database:ro",
            f"{dirname}:/output:rw",
        )
    )
    output_extension = extension.replace('fastq', 'fastq-mcf')
    command = (
        f"fastq-mcf -t 0.001 -U -R -q 0 -0 -m 5 -l {length} "
        f"-w 0 -o /output/{r1_base}.{output_extension} "
        f"{f'-o /output/{r2_base}.{output_extension}' if r2_base else ''} "
        f"/database/illumina_adapters.fa /output/{r1_base}.{extension} "
        f"{f'/output/{r2_base}.{extension}' if r2_base else ''}"
    )
    return (service, service.execute(command, stdout=stdout))


def filter_phix(sample, dirname, r1_base, r2_base, extension, stdout,
                samplesheet):
    """Runs bowtie2 on a sample.

    Tool is in bowtie2-2.6:1.0 docker.

    Arguments:
        * sample
        * dirname
        * r1_base
        * r2_base
        * extension
        * stdout
        * samplesheet
    """
    service = Service(
        client=DOCKER_CLIENT,
        image_name='fs02.compute.local:5000/baseclear/bowtie2-2.2.6:1.0',
        name=f"phix_{sample['Sample_Project']}",
        use_fast_nodes=samplesheet.is_novaseq,
        mounts=(
            "/mnt/nfs/pipelines/illumina_demultiplexing/phix:/databases:ro",
            f"{dirname}:/output:rw",
        ),
        ncpu=8
    )
    mcf_extension = extension.replace('fastq', 'fastq-mcf')
    output_extension = extension.replace('fastq', 'filt.fastq')
    format_input = (
        (
            f'-U /output/{r1_base}.{mcf_extension} ',
            f"--un-gz /output/{r1_base}.{output_extension}",
        )
        if not r2_base else
        ((
            f"-1 /output/{r1_base}.{mcf_extension} "
            f"-2 /output/{r2_base}.{mcf_extension} "
        ), (
            f"--un-conc-gz /output/{r1_base.replace('_R1_', '_R%_')}.{output_extension} "
        ))
    )
    command = (
        f"bowtie2 -x /databases/bowtie2-index_phix {format_input[0]} "
        f"-S /output/{r1_base}.sam --threads 8 --quiet "
        f"--local --very-fast-local {format_input[1]}"
    )
    return (service, service.execute(command, stdout=stdout))


def fast_qc(sample, dirname, r1_base, r2_base, extension, stdout, samplesheet):
    """Runs fastqc on a sample.

    If the run is novaseq, then fastqc tool is in fastqc:0.11.8, otherwise, the
    tool is in fastqc0.11.5:1.0

    Arguments:
        * sample
        * dirname
        * r1_base
        * r2_base
        * extension
        * stdout
        * samplesheet
    """
    service = Service(
            client=DOCKER_CLIENT,
            image_name=(
                'fs02.compute.local:5000/baseclear/fastqc:0.11.8'
                if samplesheet.is_novaseq else
                'fs02.compute.local:5000/baseclear/fastqc0.11.5:1.0'
                ),
            name=f"fastqc_{sample['Sample_Project']}",
            ncpu=2,
            use_fast_nodes=samplesheet.is_novaseq,
            mounts=(f"{dirname}:/output:rw",),
            # verbose=True,
            # autoremove=False
    )
    extension = f"{'filt.' if sample['Recipe'] != 'no-filt' else ''}fastq.gz"
    command = (
        f"fastqc -q -t 2 --nogroup --outdir /output "
        f"/output/{r1_base}.{extension} "
        f"{f'/output/{r2_base}.{extension}' if r2_base else ''}"
    )
    return (service, service.execute(command, stdout=stdout))


def md5sum(arguments, data_folder, samplesheet):
    """Run mdsum on the .gz files."""
    LOGGER.log("Performing MD5 hashing")
    image_name = 'fs02.compute.local:5000/baseclear/ubuntu:16.04'
    jobs = []
    # This will break. now multiple folders...
    for project_meta in samplesheet.projects:
        expression = f"*{'filt.' if project_meta['Recipe'] != 'no-filt' else ''}fastq.gz"
        file_expression = os.path.join(data_folder,
                                       project_meta['Sample_Project'],
                                       expression)
        files = glob.glob(file_expression)
        if files:
            project_directory = os.path.dirname(files[0])
            service = Service(client=DOCKER_CLIENT,
                              image_name=image_name,
                              name=f"md5sum_{project_meta['Sample_Project']}",
                              ncpu=1,
                              use_fast_nodes=samplesheet.is_novaseq,
                              mounts=(f"{project_directory}:/output:rw",),
                              workdir="/output")
            output_file = (f"md5sum_demultiplexing_"
                           f"{project_meta['Sample_Project']}_"
                           f"{project_meta['Operator']}_"
                           f"{project_meta['FCID']}.txt")
            command = (f"/bin/sh -c 'md5sum "
                       f"{expression}"
                       f" > {output_file}'"
            )
            # CHECK: is there a reason we send stdout to the main logger?
            jobs.append(
                (service, service.execute(command, stdout=LOGGER.stdout))
            )
        else:
            if not files:
                LOGGER.warn(
                    f"No files were found that match the expression: "
                    f"{file_expression}. Skipping md5 generation.")
    Service.wait_for_services(jobs)


def unzip_fastq(sample, dirname, r1_base, r2_base, extension, stdout,
                samplesheet):
    """Runs pigz tool for a sample.

    pigz is in the pigz:1.0 docker

    Arguments:
        * sample
        * dirname
        * r1_base
        * r2_base
        * extension
        * stdout
        * samplesheet
    """
    extension = f"{'filt.' if sample['Recipe'] != 'no-filt' else ''}fastq.gz"
    service = Service(
        client=DOCKER_CLIENT,
        use_fast_nodes=samplesheet.is_novaseq,
        image_name='fs02.compute.local:5000/baseclear/pigz:1.0',
        name=f"unzip_{sample['Sample_ID']}",
        ncpu=2,
        mounts=(f"{dirname}:/output:rw",)
    )
    command = (
        f"pigz -d -p 2 -k --force /output/{r1_base}.{extension} "
        f"{f'/output/{r2_base}.{extension}' if r2_base else ''}"
    )
    return (service, service.execute(command, stdout=stdout))


def cleanup(arguments, data_folders):
    """"""
    for folder in data_folders:
        _cleanup(folder)


def _cleanup(folder):
    """Deletes temporary and unecesary files.

    Arguments:
        * arguments (obj:`Namespace`): only using the abs_output_folder from the
          object to cleanup files from this directory.
    """
    # TODOL: check for the abs_output_folder is no longer required...
    #TODO: Use filemover for this
    LOGGER.log(f"Cleaning up '{folder}'.")
    backup_dir = os.path.join(folder,
                              f"ReportsAndStats_{Service.id_generator()}")
    reports_dir = os.path.join(folder, "Reports")
    stats_dir = os.path.join(folder, "Stats")
    if not os.path.isdir(backup_dir) and (
        os.path.isdir(reports_dir) or os.path.isdir(stats_dir)):
        os.mkdir(backup_dir)

    if os.path.isdir(reports_dir):
        shutil.move(reports_dir, os.path.join(backup_dir, "Reports"))

    if os.path.isdir(stats_dir):
        shutil.move(stats_dir, os.path.join(backup_dir, "Stats"))

    for item in os.listdir(folder):
        if "ReportsAndStats" not in item:
            item = os.path.join(folder, item)
            try:
                shutil.rmtree(item)
            except OSError as err:
                try:
                    os.remove(item)
                except OSError:
                    LOGGER.warn(f"Couldn't remove dir {item}, {str(err)}")


def setup_summary_table(datadir, flowcell_id):
    """Prepare data for the generation of the summary file.

    This method looks at the results from the bcl2fastq
    Arguments:
        * datadir (str): a string representing the data directory.
        * flowcell_id (str): a string representing a flowcell.

    Returns: A pandas dataframe with data from averageQualityPerSample.ps script
    """
    html_files = glob.glob(os.path.join(datadir,
                                        "Reports/html",
                                        f"*{flowcell_id}",
                                        "all/all/all/laneBarcode.html"))
    results = pd.DataFrame()

    command = (
        f'perl /mnt/nfs/pipelines/illumina_demultiplexing/averageQualityPerSample.pl '
        f'{os.path.join(datadir, "*", "*fastqc/fastqc_data.txt")}'
    )
    p = subprocess.Popen(
        command, shell=True, stdout=subprocess.PIPE)

    out, err = p.communicate()
    if p.returncode != 0:
        raise subprocess.CalledProcessError(p.returncode, p.args)

    data = pd.read_csv(
        io.StringIO(out.decode()),
        sep="\t", names=["Sample", "nrReads", "YieldMb", "avgQ"]
    ).rename(columns={"Sample": "Sample_ID"})

    for file in html_files:
        html_data = LaneBarcode([file]).lane_summary

        if (len(html_data) - 1) != len(data):
            # This will print per file...
            LOGGER.warn(
                f"Conflicting number of samples found in report html ({len(html_data)}) "
                f"and by averageQualityPerSample.pl ({len(data)})"
            )
        cur_results = pd.merge(html_data, data, on='Sample_ID', how='left')
        results = results.append(cur_results) if not results.empty else cur_results
    return results


def process_fastqc_data(samplesheet, data_folder, flowcell_id, tempdata):
    """"""
    data = setup_summary_table(data_folder, flowcell_id)
    tempdata = pd.DataFrame(tempdata, columns=["Sample_ID", "nFails", "fqcFails", "GC"])

    # fails is obsolete after Qdoc change, but ET uses this as a column
    tempdata[['nFails', "GC"]] = tempdata[['Sample_ID', 'nFails', "GC"]].groupby("Sample_ID").transform('mean')

    samplesheet.data = pd.merge(
        samplesheet.data, pd.merge(data,
                                   tempdata.drop_duplicates(subset="Sample_ID"),
                                   on='Sample_ID',
                                   how='outer'),
        on=['Sample_ID', "Lane", "Sample_Project"],
        how='outer')


def generate_statistics(run_folder, data_folder, flowcell_id, samplesheet):
    """Generate statistics for the run.

    Arguments:
        * arguments
        * run_info
        * samplesheet

    Gather the results from the samplesheet object. Also writes this statistics
    to xml and excel

    Returns: A list of excel files generated.
    """
    tempdata = []
    LOGGER.log("Generating Statistics")
    for index, sample in samplesheet.data.iterrows():
        sample_pattern = os.path.join(data_folder,
                                      f"{sample['Sample_Project']}",
                                      f"{sample['Sample_ID']}*zip")
        for zfile in glob.glob(sample_pattern):
            zdir = zfile.replace(".zip", "")

            try:
                if not os.path.isdir(zdir):
                    with zipfile.ZipFile(zfile) as zip_file:
                        zip_file.extractall(os.path.dirname(zdir))
            except zipfile.BadZipFile as ex:
                if "empty" in zfile:
                    LOGGER.warn(f"Empty zip file found... skipping.")
                    continue
                LOGGER.log("Failed to unzip {zfile} it seems to be a bad zip"
                           "file.")
                raise ex

            if os.path.isdir(zdir):
                with open(os.path.join(zdir, "summary.txt"), "r") as summary:
                    fastqc_fails = [str(i) for i,j in enumerate(summary.readlines()) if j.startswith("FAIL")]
                    n_fails = len(fastqc_fails)
                with open(os.path.join(zdir, "fastqc_data.txt"), "r") as info_file:
                    gc = [
                        line.split()[-1] for line in info_file.readlines()
                        if line.startswith(f"%GC")
                    ]
                tempdata.append(
                    [sample['Sample_ID'], n_fails,
                    '-'.join(fastqc_fails), int(gc[0]) if len(gc) == 1 else 'Na']
                )

    # TODO: check if this behaves as expected....
    process_fastqc_data(samplesheet, data_folder, flowcell_id, tempdata)

    # This writes to the report directory. For easyness, move it to the run
    # folder.
    samplesheet.write_lane_summary(run_folder, flowcell_id)

    # Write reports to the "report" directory.
    samplesheet.write_sample_xmls()

    return samplesheet.write_excel(flowcell_id)


def move_lowyield_samples(project,
                          samples,
                          samplesheet,
                          data_folder,
                          destination,
                          suffix="_awaitingAdditionalData"):
    """Move raw data and reports for a sample.

    This is usually used when you dont have sufficient yiled for a sample.

    Precondition:
        * Expect that samples is a non empty list.

    Arguments:
        * project (:obj:`pd.DataFrame`): project data.
        * samples(:obj:`list`): A list of samples to move.
        * destination (:str:): Destination where files will be moved.
        * suffix (`str`):  a suffix to be added to the destination.
    """
    project_dir = os.path.join(data_folder, project['Sample_Project'])
    dest_project_dir = get_project_dir(project['Sample_Project'], "", destination, "")
    if not dest_project_dir:
        dest_project_dir = os.path.join(os.path.realpath(destination),
                                        f"{project['Sample_Project']}_{project['Operator']}")
    dest_reports_dir = os.path.join(dest_project_dir, "reports",
                                    f"illumina_{project['FCID']}{suffix}")
    dest_raw_sequences = os.path.join(dest_project_dir,
                                      "raw_sequences",
                                      f"{project['FCID']}{suffix}")

    raw_sequences_patterns = list(set([
        f"{sample}_*{'filt.fastq*' if project['Recipe'] != 'no-filt' else 'fastq.gz'}"
        for sample in samples
    ]))
    report_patterns = list(set([
        f"{sample}_{project['Sample_Project']}_demultiplexing-report.xml"
        for sample in samples
    ]))

    with FileMover(project_dir, dest_raw_sequences) as file:
        raw_data_moved = file.move_with_pattern(raw_sequences_patterns)
        len_md5sum = file.move_with_pattern("md5sum*", copy=True)

    with FileMover(samplesheet.reports_directory, dest_reports_dir) as file:
        reports_moved = file.move_with_pattern(report_patterns)

    LOGGER.log(
        f"Moved samples with low_yield: {project['Sample_Project']}, "
        f"{raw_data_moved}x data files, {reports_moved}x report files, "
        f"{len_md5sum}x md5 sum copied"
        )


def get_rivm_credentials(**kwargs):
    """get rivm credentials from the kwargs."""
    # TODO: need to *use* port...
    if kwargs.get("validation", True):
        host = kwargs.get('host', VALIDATION_RIVM_SFTP_HOST)
        port = kwargs.get('port', 22)
        username = kwargs.get('username', VALIDATION_RIVM_SFTP_USER)
        password = kwargs.get('password', VALIDATION_RIVM_SFTP_PASSWORD)
    else:
        host = kwargs.get('host', RIVM_SFTP_HOST)
        port = kwargs.get('port', 21)
        username = kwargs.get('username', RIVM_SFTP_USER)
        password = kwargs.get('password', RIVM_SFTP_PASSWORD)
    return {'host': host, 'username': username, 'password': password}


def copy_to_rivm(source_dir, project, fcid, **kwargs):
    """Copy files to rivm sftp.

    Arguments:
        * source_dir (str): a string representing the location where the
          files to be moved are located. This usually is the
          abs_output_folder/<project_number>
        * project (str): a string representing the project number.
        * fcid: the fcid used to generate the data.
        * kwargs (dict): a dictionary for extra arguments. It will try to
          parse this dictionary to use custom sftp credentials (host,
          username, password, port)
    """
    sftp_kwargs = get_rivm_credentials(**kwargs)
    if kwargs.get('validation'):
        target = os.path.join(RIVM_VALIDATION_TARGET,
                              project,
                              "raw_sequences",
                              fcid)
    else:
        target = os.path.join("/", project, "raw_sequences", fcid)
    data_expression = kwargs.get('expression', "*.filt.fastq.gz")
    files = glob.glob(os.path.join(source_dir, data_expression))
    files.extend(glob.glob(os.path.join(source_dir, "md5sum_*")))
    with pysftp.Connection(**sftp_kwargs) as connection:
        try:
            if not connection.isdir(target):
                connection.makedirs(target, mode=700)
            with connection.cd(target):
                [connection.put(file) for file in files]
        except PermissionError:
            LOGGER.log(f"Unable to copy files for '{project}' due to permission errors")


def move_files(arguments, run_info, samplesheet, data_folder, ngs_folder, op_folder):
    base_op_folder = op_folder
    for project_meta in samplesheet.projects:
        # check if project dir exists...
        project_dir = get_project_dir(project_meta['Sample_Project'],
                                      "",
                                      ngs_folder,
                                      "")
        if not project_dir:
            project_dir = os.path.join(ngs_folder,
                                       f"{project_meta['Sample_Project']}"
                                       f"_{project_meta['Operator']}")
        reports_dir = os.path.join(project_dir,
                                   "reports",
                                   f"illumina_{project_meta['FCID']}")
        raw_sequences = os.path.join(project_dir,
                                     "raw_sequences",
                                     project_meta['FCID'])
        fastqc_dir = os.path.join(project_dir,
                                  "fastqc_analysis",
                                  project_meta['FCID'])
        source_dir = os.path.join(data_folder, project_meta['Sample_Project'])

        if len(glob.glob(os.path.join(
            source_dir,
                f"*{'filt.fastq*' if project_meta['Recipe'] != 'no-filt' else 'fastq.gz'}")
            )) == 0:
            LOGGER.log(f"No fastq files found for project {project_meta['Sample_Project']}, skipping")
            continue

        # copies all files to OP before moving to NGS folder.
        if arguments.copy_to_op:
            op_folder = os.path.join(base_op_folder,
                                     project_meta['Sample_Project'])
            with FileMover(samplesheet.reports_directory,
                    os.path.join(op_folder, "reports")) as handle:
                len_xml = handle.move_with_pattern(
                    f"*{project_meta['Sample_Project']}_demultiplexing-report.xml",
                    copy=True
                )
                LOGGER.log(
                    f"Copied {len_xml} xml files "
                    f"to Order Portal for project: {project_meta['Sample_Project']} "
                )
            if (not arguments.rivm_projects or project_meta['Sample_Project'] not in arguments.rivm_projects):
                with FileMover(source_dir, os.path.join(
                        op_folder, "raw_sequences")) as handle:
                    len_f = handle.move_with_pattern(
                        f"*{'filt.fastq.gz' if project_meta['Recipe'] != 'no-filt' else 'fastq.gz'}",
                        copy=True
                    )
                    len_md5sum = handle.move_with_pattern("md5sum*", copy=True)
                    LOGGER.log(
                        f"Copied {len_f} fastq.gz files and {len_md5sum} file "
                        f"to Order Portal for project: {project_meta['Sample_Project']} "
                    )
            else:
                copy_to_rivm(source_dir,
                             project_meta['Sample_Project'],
                             project_meta['FCID'],
                             validation=arguments.validation)

        LOGGER.log(f"Moving files to NGS_Projects for project {project_meta['Sample_Project']}")
        with FileMover(source_dir, raw_sequences) as handle:
            # WARNING: if you change the merge function, you may have to change
            # this. We expect that the original data is in a folder in the
            # source_dir of the format 'original_{FCID}'
            unmerged_dir = glob.glob(os.path.join(source_dir, f"original_{run_info.flowcell_id}"))
            len_fastq = handle.move_with_pattern(
                f"*{'filt.fastq*' if project_meta['Recipe'] != 'no-filt' else 'fastq.gz'}")
            handle.move_with_pattern(unmerged_dir) if unmerged_dir else None
            len_md5sum = handle.move_with_pattern("md5sum*")

        with FileMover(source_dir, fastqc_dir) as handle:
            len_fastqc = handle.move_with_pattern("*fastqc")

        with FileMover(samplesheet.reports_directory, raw_sequences) as handle:
            len_sum = handle.move_with_pattern(
                f"{project_meta['Sample_Project']}_summary-demultiplexing_{project_meta['FCID']}.xlsx", copy=True
            )
        with FileMover(samplesheet.reports_directory, reports_dir) as handle:
            len_xml = handle.move_with_pattern(
                f"*{project_meta['Sample_Project']}_demultiplexing-report.xml"
            )
        LOGGER.log(
            f"Files moved to NGS_Projects for project: "
            f"{project_meta['Sample_Project']}, {len_fastq}x fastq, "
            f"{len_md5sum}x md5sum, {len_sum}x summary file, "
            f"{len_xml}x report xml, {len_fastqc}x fastqc dir"
        )


def get_project_filters(samplesheet, arguments):
    """Read the list from --filters and map data required per project.

    Arguments:
        projects (obj: `lst`): A list of unique projects.
        filter_list (obj:`lst`): A list of [<project>, <min_data>, ...]

    Raises
        * IndexError: when the 'last' project does not have min data.
    Returns:
        * A dictionary mapping a project and min data required in mb.

    """
    # TODO:
    # * check if data is given after a project.
    # * check if the list also contiains a percentage.
    if not arguments.filters:
        return {}
    if len(arguments.filters) == 1:
        # if there is one element, then all projects have the same filter.
        return {project: arguments.filters[0] for project in samplesheet.data_project_ids}

    elif len(arguments.filters) > 1:
        return {
            arguments.filters[i]: arguments.filters[i+1]
            for i in range(0, len(arguments.filters), 2)
            if arguments.filters[i] in samplesheet.data_project_ids
        }
    return {}


def get_low_yield_samples(samplesheet, filters):
    """Get samples that have low yield.

    Arguments:
        * min_data (obj:`int`): min data required per sample.
        * data (obj:`pd.DataFrame`): result data from a project.

    Returns:
        * a list of sample names that do not meet the min_data requirement.
    """
    return {
        project: samplesheet.data[
            (samplesheet.data['Sample_Project'] == project)
            & (samplesheet.data["YieldMb"] < float(mindata))
        ][['Code', 'Sample_ID','YieldMb']].values for project, mindata in filters.items()
    }


def merge_additional_data(samplesheet, arguments, demultiplexed_files, ngs_folder, cur_flowcell):
    LOGGER.log("Merging data")
    if arguments.merge_dirs:
        dirs_to_merge = {
            arguments.merge_dirs[i]: arguments.merge_dirs[i+1]
            for i in range(0, len(arguments.merge_dirs), 2)
            if arguments.merge_dirs[i] in samplesheet.data_project_ids
        }
        jobs = []
        for project_id, directory in dirs_to_merge.items():
            merge_dir = get_project_dir(f"{project_id}",
                                        directory,
                                        ngs_folder)
            if not os.path.exists(merge_dir):
                LOGGER.warn(f"{merge_dir} Does not exist!")
                continue

            files_R1, found_samples, _ = find_files(
                os.path.join(merge_dir, "*R1_001*.fastq.gz"),
                samplesheet, False, False
            )
            files_R2, _, _ = find_files(
                os.path.join(merge_dir, "*R2_001*.fastq.gz"),
                samplesheet, False, False
            )

            for sample_id, file in files_R1.items():
                # TODO: watch out for none in filenames....
                sample = found_samples[found_samples["Sample_ID"] == sample_id].iloc[0]
                extension = f"{'filt.' if sample['Recipe'] != 'no-filt' else ''}fastq.gz"
                output_extension = extension.replace(".gz", "")
                dirname, r1_base, _ = demultiplexed_files.get(sample_id)
                r2 = files_R2.get(sample_id)
                r2_base = r1_base.replace('_R1_', '_R2_') if r2 else None

                new_location = os.path.join(dirname, f"original_{cur_flowcell}")
                r1_file = os.path.join(new_location, f"{r1_base}.{extension}")
                r2_file = os.path.join(new_location, f"{r2_base}.{extension}")

                if os.path.exists(r1_file) or os.path.exists(r2_file):
                    LOGGER.log(f"Merged files {' or '.join([r1_file, r2_file])} aleady exist. skipping")
                    continue

                original_files_patterns = []
                original_files_patterns.append(f"{os.path.join(dirname, r2_base)}.{extension}")
                original_files_patterns.append(f"{os.path.join(dirname, r1_base)}.{extension}")

                # LOGGER.log(f"Saving original samples: {original_files_patterns}")

                _save_original_file(dirname, original_files_patterns,
                                    non_merged_dirname=f"original_{cur_flowcell}")

                # LOGGER.log(f"Merging sample {sample['Sample_ID']}")

                service = Service(
                    client=DOCKER_CLIENT,
                    image_name='fs02.compute.local:5000/baseclear/ubuntu:16.04',
                    name=f"merge_{sample['Sample_ID']}",
                    ncpu=3,
                    use_fast_nodes=samplesheet.is_novaseq,
                    mounts=(
                        f"{dirname}:/output:rw",
                        f"{merge_dir}:/input:rw",
                    )
                )
                command = (
                    f"zcat /input/{file[1]}.{file[2]} "
                    f"/output/{r1_base}.{extension} "
                    f" > output/{r1_base}.{output_extension} "
                    f"&& gzip -f output/{r1_base}.{output_extension}"
                )

                # since we are using novaseq, chances are that we are always
                # getting a r2. I think this was done for when we had single
                # reads...
                if r2:
                    command += (
                        f"&& zcat /input/{r2[1]}.{r2[2]} "
                        f"/output/{r2_base}.{extension} > "
                        f"/output/{r2_base}.{output_extension} "
                        f"&& gzip -f output/{r2_base}.{output_extension}"
                    )

                jobs.append(
                    (service, service.execute(f"/bin/sh -c '{command}'" , stdout=LOGGER.stdout))
                )
        Service.wait_for_services(jobs)


def _save_original_file(dirname, patterns, non_merged_dirname="non_merged"):
    """Copy original files to a non-merged location."""
    new_location = os.path.join(dirname, non_merged_dirname)
    # LOGGER.log(f"Copying '{patterns}' to '{new_location}'.")
    with FileMover(dirname, new_location) as mover:
        mover.move_with_pattern(patterns, copy=True)


def move_samples_with_low_yield(filters, samplesheet, data_folder, ngs_folder):
    """Move files with low results."""
    LOGGER.log(f"Found {len(filters)} projects that have samples with low yield")
    for project_number, sample_list in filters.items():
        project = [x for x in samplesheet.projects if x['Sample_Project'] == project_number][0]
        if sample_list.size:
            # need to ensure that the sample list is not empty.
            move_lowyield_samples(project,
                                  sample_list[:,1],  # expect second column to contain sample names
                                  samplesheet,
                                  data_folder,
                                  ngs_folder)


def run(arguments, log_file):
    """Sequencial steps required for BC illumina demultiplexing."""

    # determine destination folders based on argument.
    ngs_folder = VALIDATION_NGS_FOLDER if arguments.validation else NGS_FOLDER
    op_folder = VALIDATION_OP_FOLDER if arguments.validation else OP_FOLDER
    LOGGER.log(f"NGS Folder: {ngs_folder}")
    LOGGER.log(f"OP_Folder: {op_folder}")
    LOGGER.log(f'sequence data folder {arguments.output_folder}')

    if not is_run_completed(arguments):
        sys.exit(f"Run {arguments.run_folder} is not completed yet.")

    samplesheet, run_info, checked_samplesheet_paths = generate_samplesheets(arguments)

    output_folders = []
    ss_data = {}
    for ss_checked in checked_samplesheet_paths:
        # index string is to be used as the basename of the output-folder
        # (Data/Intensities/Basecalls/<index_string>)
        index_string = ss_checked.split('_')[-1].split('.')[0]
        of = os.path.join(arguments.output_folder, index_string)
        output_folders.append(of)
        ss_data[ss_checked] = of

    bcl2fastq(arguments.run_folder,
              run_info,
              ss_data,
              samplesheet.is_novaseq,
              mismatches=arguments.mismatches,
              extra_opts=arguments.extra_opts,
              logdir=arguments.log_dir)

    check_undetermined_and_stats(arguments.run_folder,
                                 output_folders,
                                 run_info,
                                 samplesheet,
                                 is_validation=arguments.validation,
                                 logdir=arguments.log_dir)

    # CHECK: does this already exists?
    data_folder = os.path.join(arguments.run_folder, arguments.output_folder)
    # This is abs_output_folder...
    pattern = os.path.join(arguments.run_folder,
                           arguments.output_folder,
                           "*",
                           "*",
                           "*R1_001*.fastq.gz")
    demultiplexed_files, found_samples, filt_samples = find_files(pattern,
                                                                  samplesheet)

    # CHECK: adapter_length is not being used in the trim_adpters method...
    sample_loop(arguments, samplesheet, filt_samples, demultiplexed_files, trim_adapters, arguments.adapter_length)

    sample_loop(arguments, samplesheet, filt_samples, demultiplexed_files, filter_phix)

    merge_additional_data(samplesheet, arguments, demultiplexed_files, ngs_folder, run_info.flowcell_id)

    sample_loop(arguments, samplesheet,  found_samples, demultiplexed_files, fast_qc)

    md5sum(arguments, os.path.join(data_folder, "*"), samplesheet)

    if not samplesheet.is_novaseq:
        sample_loop(arguments, samplesheet, found_samples, demultiplexed_files, unzip_fastq)

    stats_files = generate_statistics(arguments.run_folder,
                                      os.path.join(data_folder, "*"),
                                      run_info.flowcell_id,
                                      samplesheet)

    filters = get_project_filters(samplesheet, arguments)
    low_yield_samples = get_low_yield_samples(samplesheet, filters)
    move_samples_with_low_yield(low_yield_samples,
                                samplesheet,
                                os.path.join(data_folder, "*"),
                                ngs_folder)

    move_files(arguments,
               run_info,
               samplesheet,
               os.path.join(data_folder, "*"),
               ngs_folder,
               op_folder)

    cleanup(arguments, glob.glob(os.path.join(data_folder, "*")))

    title = "Validation Demultiplexing Finished" if arguments.validation else "Demultiplexing Finished"
    send_email(create_email_content(samplesheet,
                                    filters,
                                    low_yield_samples,
                                    run_info.flowcell_id),
               title,
               WATCHER_EMAILS,
               attachments=stats_files + [log_file])


def create_email_content(samplesheet, filters, low_yield_samples, flowcell_id):
    """Create a message string to be sent on email.

    WARNING: values in low_yield_samples are expected in certain order.
    See low_yiled_samples for the order that columns are returned.
    """
    projects_wo_filter = set(samplesheet.data_project_ids) - set(filters.keys())
    message = (f"Demultiplexing finished for {flowcell_id}<br>"
               f"Projects in this run: {', '.join(samplesheet.data_project_ids)}<br>")
    for project in samplesheet.data_project_ids:
        message += "<br>"
        if project not in filters.keys():
            message += f"No data requirement was provided for project {project}"
            continue
        message += f"Project {project} requires {filters[project]}Mb.<br>"
        if project not in low_yield_samples.keys():
            message += f"All samples have enough data.<br>"
            continue
        header = "<tr><th>Code</th><th>Required Data</th><th>Yield</th></tr>"
        message += f"The following samples did not yield enough data:<br><table>{header}"
        for sample_data in low_yield_samples[project]:
            message += f"<tr><td>{sample_data[0]}</td><td>{sample_data[1]}</td><td>{sample_data[2]}</td></tr>"
        message += "</table>"
    return message

def cli():
    arguments = get_required_arguments()
    return arguments


if __name__ == '__main__':
    """Entry point of script.
    """
    arguments = cli()
    log_file = os.path.join(arguments.log_dir, "logs.txt")
    LOGGER = Logger(log_file)
    # DOCKER_CLIENT.login(
    #     username="prod",
    #     registry="http://fs02.compute.local:5000/v2/",
    # )
    LOGGER.log(f'Running: {" ".join(sys.argv)}')
    try:
        run(arguments, log_file)
    except Exception as error:
        if not arguments.validation:
            ERROR_CLIENT.captureException()
            LOGGER.error(str(error))
        raise error
